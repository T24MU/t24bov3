<!DOCTYPE html>
<html>
<head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
@media print
{    
    .no-print, .no-print *
    {
        /*display: none !important;*/
        visibility:hidden;
    }
}
@page {
        size: 21cm 28.7cm;
        margin: 12mm 7mm 7mm 7mm;   /* PDF
        change the margins as you want them to be. */

   /* Print
   margin: 5mm 8mm 8mm 10mm; 
   size: 21cm 29.7cm; change the margins as you want them to be. */
}
body {
        background-color:#525659;
        -webkit-font-smoothing: antialiased; width: 100% !important; -webkit-text-size-adjust: none;
        /*
        height: 542px;
        width: 100%;
         to centre page on screen
        margin-left: auto;
        margin-right: auto;
        */
        font-family: Arial;
        -webkit-print-color-adjust: exact;
}
#content {
    display: table;
    width:755px;
}
/*Printing dimensions
@page {
    */
        /*size:A4; margin: 5mm; /* border:1pt solid blue; */
        /*size: 31cm 39.7cm;*/
        /*margin: 10mm 25mm 10mm 25mm; */ /* change the margins as you want them to be. */
/*}*/
.page_color {
        background-color:#ffffff;
        text-align:center;
        margin:0 auto;
}
.heading_banner {
        background-color:#44a794;
        color:white;
        font-size:12px;
}
.page_break{
        /*width:100%;
        position:relative;
        height:20px;
        */
        page-break-after: always;
}
.page_break_before{
        /*width:100%;
        position:relative;
        height:20px;
        */
        page-break-before: always;
}
.pie_options {
        position:relative;top:1px;width:10px;height:10px;border: solid 1px #000000;margin:1px;
}
.pie_c_frds_red{
        background-color:#dc3912;
}
.pie_c_frds_blue{
        background-color:#3366cc;
}
.blank_line {
        width:100%;height:20px;background-color:#ffffff;
}
.blank_line_trans {
        width:100%;height:7px;
}
.transact24_heading_logo {
        position:relative;margin-top:30px;width:40%;padding:0px;margin-left:20px;text-align:left;left:10px;
}
.heading_monthly_stm{
        position:relative;top:3px;margin-top:40px;width:50%;text-align:right;font-family:helvetica;font-size:22px;font-weight:normal;color:#44a794;right:10px;
}
.chart_title_font{
        font-family:Arial;font-size:11px;color:#877171;margin-left:5px;
}
.chart_subtitle_font{
        font-family:Arial;font-size:9px;color:#44a794;font-weight:bold;margin-left:5px;
}
tr.spaceUnder>td {
        /*padding-bottom: 0px; */
        line-height:12px;
}
td.textLeft{
        position:relative;text-align:left;
}
td.textCenter{
        position:relative;text-align:center;
}
#graph_zero {
        background-image:url('images/graph_0_percent_white.png');
        width:160px;
        height:140px;        
}
</style>

<?php

//include $_SERVER["DOCUMENT_ROOT"].'/connect.php';


include $_SERVER["DOCUMENT_ROOT"].'../../connect.php';
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );

$path = "";

$bankTimezone = "Europe/Riga";

if (isset($_GET['timezone'])) {

        $bankTimezone = urldecode($_GET['timezone']);
        //echo 'TIMEZONE = '.$tz;
}

$merchantid = $_POST['merchant'];

$monthlystatfee = $_POST['monthlystatfee'];
$confirmstatementbox = $_POST['confirmstatementbox'];
$graphics_yes = $_POST['graphics_yes'];

$daylight_savings_on = "NO";

if(isset($_POST['daylightsavings']) ) {
        $daylight_savings_on = "YES";
}

//$daylight_savings_on = $POST['daylightsavings'];
//$daylight_savings_on = ($_POST['daylight_savings_on'] == 'YES')?'YES':'NO';
$wirefee = $_POST['wirefee'];
$sentity = $_POST['sentity'];
$adj_note = $_POST['adj_note'];
$adj_items = $_POST['adj_items'];
$adj_auth_amt = $_POST['adj_auth_amt'];
$adj_settle_amt = $_POST['adj_settle_amt'];

//Message heading
$message_heading_id = $_POST['heading_opt'];
$result = pg_query($pg_conn, "SELECT message from statement_message WHERE message_id = ".$message_heading_id);
$heading_row = pg_fetch_row($result);
$message_heading = $heading_row[0];

//Message primary
$message_primary_id = $_POST['primary_opt'];
$result = pg_query($pg_conn, "SELECT message from statement_message WHERE message_id = ".$message_primary_id);   
$primary_row = pg_fetch_row($result);
$message_primary = $primary_row[0];

//Message primary note
$message_primary_note = $_POST['primary_note'];

//Message Secondary
$message_secondary_id = $_POST['secondary_opt'];
$result = pg_query($pg_conn, "SELECT message from statement_message WHERE message_id = ".$message_secondary_id);   
$secondary_row = pg_fetch_row($result);
$message_secondary = $secondary_row[0];

$stattype_radio = $_POST['stattype_radio'];
$fromdate = date("Y-M-d", strtotime($_POST['fromdate']) );
$todate = date("Y-M-d", strtotime($_POST['todate']) );
$rrfromdate = date("Y-M-d", strtotime($_POST['rrfromdate']) );
$rrtodate = date("Y-M-d", strtotime($_POST['rrtodate']) );

//loop in merchant table to read bitrix merchant id
$result = pg_query($pg_conn, "SELECT s.statement_no, mm.merchant_mapping_id, m.merchant_id, mm.merchant_bitrix_id, m.merchant_name, mm.merchant_mapping_ref_code, mm.merchant_mapping_name_recon, s.statement_id  FROM merchant m , merchant_mapping mm, statement s where s.statement_active = 1 and m.merchant_id = mm.merchant_t24_id and mm.merchant_mapping_active = 1 and mm.merchant_t24_id = ".$merchantid." and s.statement_merchant_mapping_id = mm.merchant_mapping_id group by mm.merchant_mapping_id, m.merchant_id, mm.merchant_bitrix_id, m.merchant_name, mm.merchant_mapping_ref_code, mm.merchant_mapping_name_recon, s.statement_id, s.statement_no order by s.statement_id DESC limit 1");   

$merchant = pg_fetch_array($result);
$assoc_array = array();

if (!empty($merchant))
{

        $my_merchant_ref_code = $merchant['merchant_mapping_ref_code'];

        //echo $merchant['merchant_bitrix_id']."'>". $merchant['merchant_id']."'>".$merchant['merchant_name']."<br/>"; 
        //get all merchant details on bitrix with its merchant id
        $assoc_array['statement_id'] = $merchant['statement_id'];
        $assoc_array['merchant_mapping_id'] = $merchant['merchant_mapping_id'];
        $assoc_array['statement_old_no'] = $merchant['statement_no'];
        $assoc_array['statement_no'] = str_pad($merchant['statement_no'] + 1, 3, '0', STR_PAD_LEFT) ;
        $assoc_array['merchant_bitrix_id'] = $merchant['merchant_bitrix_id'];
        $assoc_array['merchant_id'] = $merchant['merchant_id']; 
        $assoc_array['merchant_name'] = $merchant['merchant_name'];
        //$assoc_array['merchant_mapping_ref_code'] = trim($merchant['merchant_mapping_ref_code'],'\n', '\0', '\t', '\r');
        $assoc_array['merchant_mapping_ref_code'] = trim($merchant['merchant_mapping_ref_code']);
        $my_ref_code = $merchant['merchant_mapping_ref_code'];
        $assoc_array['merchant_mapping_name_recon'] = $merchant['merchant_mapping_name_recon'];
        $assoc_array['fromdate'] = $fromdate;
        $assoc_array['todate'] = $todate;

        $app_id = "local.59ccdde14ea661.77614513";
        $client_secret_key = "D9cnMhoDbitxquZtiBjxZ7Qo56tuC78mLpu1W8EhPhaVIjn6LR";
        //$accesstoken = "k09029056j2j21xd09qpfwewi18rn0dq"; //"prev access_token":"z97waqnlyohgyn33qetne861rsogchs2"
        //$refreshtoken = "gd0r6b2d52u2gwmoc6x6sk4bu9ztiiu4"; //"prev refresh_token":"uqgu067ohxenkey45xyz2plnvjr04q1z"

        $result_token = pg_query($pg_conn, "SELECT refresh_token, access_token from token where id = 1");   
        $token = pg_fetch_array($result_token);

        $accesstoken = $token['access_token'];
        $refreshtoken = $token['refresh_token'];

        $domain = 'https://transact24.bitrix24.com';


        $res = file_get_contents($domain.'/oauth/token/?grant_type=refresh_token&client_id='.$app_id.'&client_secret='.$client_secret_key.'&refresh_token='.$refreshtoken.'&scope=crm');

        $obj = json_decode($res);
        
        //var_dump($obj); echo "<br/><br/>";

        $accesstoken = $obj->{'access_token'};
        $refreshtoken = $obj->{'refresh_token'};

        $qinsert = "UPDATE token SET refresh_token = '".$refreshtoken."', access_token = '".$accesstoken."' WHERE id = 1";
        $res_qinsert = pg_query($pg_conn, $qinsert);  
        //$row_statement_id = pg_fetch_row($res_qinsertstatementrow);

        $dealdetails = file_get_contents($domain.'/rest/crm.deal.get/?id='.$merchant['merchant_bitrix_id'].'&auth='.$accesstoken);
        $obj_dealdetails = json_decode($dealdetails);

        //company parameters
        $assoc_array['beneficiary_name'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473145166'};
        $assoc_array['company_address'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473066513'}." ".$obj_dealdetails->{'result'}->{'UF_CRM_1473066557'};
        $assoc_array['postal_code'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473066648'};
        $assoc_array['country'] = $obj_dealdetails->{'result'}->{'UF_CRM_1471942550'};
        $assoc_array['bank_route'] = $obj_dealdetails->{'result'}->{'UF_CRM_1499928327'};
        $assoc_array['mcc'] = $obj_dealdetails->{'result'}->{'UF_CRM_1487073493'};
        $assoc_array['contact_person_name'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473073331'}." ".$obj_dealdetails->{'result'}->{'UF_CRM_1473073337'};
        $assoc_array['contact_person_email'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473073356'};
        $assoc_array['website_address'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473071964'};
        $assoc_array['live_mid'] = $obj_dealdetails->{'result'}->{'UF_CRM_1497339372'};
        
        //echo $beneficiary_name ." | ".$contact_person_name." | ".$contact_person_email." | ".$company_address." | ".$postal_code." | ".$country." | ".$bank_route." | ".$mcc;

        //bank details
        $assoc_array['beneficiary_bank_name'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626754'};
        $assoc_array['beneficiary_address'] = $obj_dealdetails->{'result'}->{'UF_CRM_1473066589'};
        $assoc_array['bank_address'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626864'};
        $assoc_array['bank_name'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626850'};
        $assoc_array['beneficiary_bank_code'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626817'};
        $assoc_array['bank_swift_code'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626788'};
        $assoc_array['account_number'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626765'};
        $assoc_array['iban'] = $obj_dealdetails->{'result'}->{'UF_CRM_1472626800'};
        
        //company fees
        $assoc_array['mdr_visa'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809923'};
        $assoc_array['mdr_masterCard'] = $obj_dealdetails->{'result'}->{'UF_CRM_1496400227'};
        $assoc_array['auth_currency'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491806084'};
        $assoc_array['settlement_currency'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491806096'};
        $assoc_array['desired_settlement_currency'] = $obj_dealdetails->{'result'}->{'UF_CRM_1479970523'};
        $assoc_array['merchant_set_up_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809885'};
        $assoc_array['monthly_admin_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810050'};
        $assoc_array['transaction_fee_n3d_secure_approved'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809963'};
        $assoc_array['transaction_fee_3D_secure_approved'] = $obj_dealdetails->{'result'}->{'UF_CRM_1495523161'};
        $assoc_array['transaction_fee_n3d_secure_declined'] = $obj_dealdetails->{'result'}->{'UF_CRM_1495523183'};
        $assoc_array['transaction_fee_3d_secure_declined'] = $obj_dealdetails->{'result'}->{'UF_CRM_1495523202'};
        $assoc_array['refund_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809992'};
        $assoc_array['fraud_alert_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810002'};
        
        if ($monthlystatfee == 'monthlystatfee') 
        { $assoc_array['monthly_statement_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1496313346'}; } 
        else {$assoc_array['monthly_statement_fee'] = 0; }

        if ($wirefee == 'wirefee') 
        { $assoc_array['wire_transfer_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810039'}; } 
        else {$assoc_array['wire_transfer_fee'] = 0; }

        $assoc_array['chargeback_fee']	= $obj_dealdetails->{'result'}->{'UF_CRM_1496313305'};
        $assoc_array['chargeback_fee_more_1_percent'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810013'};
        $assoc_array['ad_hoc_integration_support_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810062'};
        $assoc_array['chargeback_representment_fee'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491810025'};
        $assoc_array['rolling_reserve'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809949'};
        preg_match( '#([0-9][0-9])%#' ,  $assoc_array['rolling_reserve'] , $matches);
        //var_dump($matches);
        $assoc_array['rolling_reserve_amt'] = $matches[1];
        $assoc_array['payout_cycle'] = $obj_dealdetails->{'result'}->{'UF_CRM_1491809934'};
        $assoc_array['time_zone'] = $obj_dealdetails->{'result'}->{'UF_CRM_1515498222'};

        //echo $mdr_visa."<br/>".$mdr_masterCard."<br/>".$auth_currency."<br/>".$settlement_currency."<br/>".$merchant_set_up_fee."<br/>".$monthly_admin_fee."<br/>".$transaction_fee_n3d_secure_approved."<br/>".$transaction_fee_3D_secure_approved."<br/>".$transaction_fee_n3d_secure_declined."<br/>".$transaction_fee_3d_secure_declined."<br/>".$refund_fee."<br/>".$fraud_alert_fee."<br/>".$monthly_statement_fee."<br/>".$chargeback_fee."<br/>".$wire_transfer_fee."<br/>".$chargeback_fee_more_1_percent."<br/>".$ad_hoc_integration_support_fee."<br/>".$chargeback_representment_fee."<br/>".$rolling_reserve."<br/>".$payout_cycle;

        //echo "<br/><br/>";
        //print_r($assoc_array); 
        //echo $isufproduct."  |  ".$rfproduct."  |  ".$fn;
}
//var_dump($assoc_array); die();
/**************** GET BALANCE BROUGHT FORWARD ******************/

$result_statement = pg_query($pg_conn, "SELECT statement_paid_to_merchant, statement_bbf_amount, statement_remit_amount FROM statement where statement_active = 1 and statement_no = ".$assoc_array['statement_old_no']." and statement_id = ".$assoc_array['statement_id']);   
$statement_info = pg_fetch_array($result_statement);
if (!empty($statement_info))
{
        if ($statement_info['statement_paid_to_merchant'] == 0 && $statement_info['statement_bbf_amount'] == 0 )
        {
                $assoc_array['statement_bbf_amount'] = $statement_info['statement_remit_amount'];
        }
        else if ($statement_info['statement_paid_to_merchant'] == 1)
        {
                 $assoc_array['statement_bbf_amount'] = 0;
        }
        else
        {
                $assoc_array['statement_bbf_amount'] = $statement_info['statement_bbf_amount'];
        }
}

/************ NUMBER OF SALES ************/

//INITIALIZE ARRAYS
$sales_array = array(); $sales_array['count'] = $sales_array['sum'] = $sales_array['recon_count'] = $sales_array['recon_sum'] =  0.00;
$sales_VISA_array = array(); $sales_VISA_array['count'] = $sales_VISA_array['sum'] = $sales_VISA_array['recon_count'] = $sales_VISA_array['recon_sum'] =  0.00;
$sales_MC_array = array(); $sales_MC_array['count'] = $sales_MC_array['sum'] = $sales_MC_array['recon_count'] = $sales_MC_array['recon_sum'] =  0.00;
$declines_array = array(); $declines_array['count'] = $declines_array['sum'] = $declines_array['recon_count'] = $declines_array['recon_sum'] =  0.00;
$refunds_VISA_array = array(); $refunds_VISA_array['count'] = $refunds_VISA_array['sum'] = $refunds_VISA_array['recon_count'] = $refunds_VISA_array['recon_sum'] =  0.00;
$refunds_MC_array = array(); $refunds_MC_array['count'] = $refunds_MC_array['sum'] = $refunds_MC_array['recon_count'] = $refunds_MC_array['recon_sum'] =  0.00;
$refunds_array = array(); $refunds_array['count'] = $refunds_array['sum'] = $refunds_array['recon_count'] = $refunds_array['recon_sum'] =  0.00;
$adjustments_array = array(); $adjustments_array['count'] = $adjustments_array['sum'] = $adjustments_array['recon_count'] = $adjustments_array['recon_sum'] =  0.00;
$fraudalers_array = array(); $fraudalers_array['count'] = $fraudalers_array['sum'] = $fraudalers_array['recon_count'] = $fraudalers_array['recon_sum'] =  0.00;
$fraudalers_VISA_array = array(); $fraudalers_VISA_array['count'] = $fraudalers_VISA_array['sum'] = $fraudalers_VISA_array['recon_count'] = $fraudalers_VISA_array['recon_sum'] = 0.00;
$fraudalers_MC_array = array(); $fraudalers_MC_array['count'] = $fraudalers_MC_array['sum'] = $fraudalers_MC_array['recon_count'] = $fraudalers_MC_array['recon_sum'] = 0.00;
$chargebacks_array = array(); $chargebacks_array['count'] = $chargebacks_array['sum'] = $chargebacks_array['recon_count'] = $chargebacks_array['recon_sum'] =  0.00;
$chargeback_reversals_array = array(); $chargeback_reversals_array['count'] = $chargeback_reversals_array['sum'] = $chargeback_reversals_array['recon_count'] = $chargeback_reversals_array['recon_sum'] =  0.00;
$chargebacks_per_month_array = array(); $chargebacks_per_month_array['count'] = $chargebacks_per_month_array['sum'] = 0.00;
$chargebacks_VISA_array = array(); $chargebacks_VISA_array['count'] = $chargebacks_VISA_array['sum'] = $chargebacks_VISA_array['recon_count'] = $chargebacks_VISA_array['recon_sum'] =  0.00;
$chargebacks_MC_array = array(); $chargebacks_MC_array['count'] = $chargebacks_MC_array['sum'] = $chargebacks_MC_array['recon_count'] = $chargebacks_MC_array['recon_sum'] =  0.00;
$representments_array = array(); $representments_array['count'] = $representments_array['sum'] = $representments_array['recon_count'] = $representments_array['recon_sum'] =  0.00;
$fraud_trns_current_month_VISA_array = array(); $fraud_trns_current_month_VISA_array['count'] = $fraud_trns_current_month_VISA_array['sum'] = 0.00;
$fraud_current_month_VISA_array = array(); $fraud_current_month_VISA_array['count'] = $fraud_current_month_VISA_array['sum'] = 0.00;
$fraud_trns_last_month_MC_array = array(); $fraud_trns_last_month_MC_array['count'] = $fraud_trns_last_month_MC_array['sum'] = 0.00;
$fraud_current_month_MC_array = array(); $fraud_current_month_MC_array['count'] = $fraud_current_month_MC_array['sum'] = 0.00;
$chargebacks_debited_array = array();
$chargebacks_trns_current_month_VISA_array = array(); $chargebacks_trns_current_month_VISA_array['count'] = $chargebacks_trns_current_month_VISA_array['sum'] = 0.00;
$chargebacks_current_month_VISA_array = array(); $chargebacks_current_month_VISA_array['count'] = $chargebacks_current_month_VISA_array['sum'] = 0.00;
$chargebacks_trns_last_month_MC_array = array(); $chargebacks_trns_last_month_MC_array['count'] = $chargebacks_trns_last_month_MC_array['sum'] = 0.00;
$chargebacks_current_month_MC_array = array(); $chargebacks_current_month_MC_array['count'] = $chargebacks_current_month_MC_array['sum'] = 0.00;

$fraud_ratio_perc_VISA_value = 0;
$fraud_ratio_perc_VISA_count = 0;
$fraud_ratio_perc_MC_value = 0;
$fraud_ratio_perc_MC_count = 0;
$chargeback_ratio_perc_VISA_value = 0;
$chargeback_ratio_perc_VISA_count = 0;
$chargeback_ratio_perc_MC_value = 0;
$chargeback_ratio_perc_MC_count = 0;

//add date + 1 because using between
$assoc_array['sel_date_from'] = date('Y-m-d H:i:s', strtotime($assoc_array['fromdate']));
$assoc_array['sel_date_to_temp'] = date('Y-m-d H:i:s', strtotime( '+1 day', strtotime($assoc_array['todate'])));
$assoc_array['sel_date_to'] = date("Y-m-d H:i:s", strtotime("-1 second", strtotime($assoc_array['sel_date_to_temp'])));

$time_BANK_timezone_from = $assoc_array['sel_date_from'];
$time_BANK_timezone_to = $assoc_array['sel_date_to'];

$time_date_from = $assoc_array['sel_date_from'];
$time_date_to = $assoc_array['sel_date_to'];

//Calculate Timezone with Daylight savings
//#######################################################################################

$userTimezone = new DateTimeZone($bankTimezone);
//$userTimezone = new DateTimeZone('Europe/Riga');
//$userTimezone = new DateTimeZone('America/New_York');

$gmtTimezone = new DateTimeZone('GMT');
$myDateTime = new DateTime('2018-03-28 08:00', $gmtTimezone);
$offset = $userTimezone->getOffset($myDateTime);
$myInterval=DateInterval::createFromDateString((string)$offset . 'seconds');
$myDateTime->add($myInterval);
$result = $myDateTime->format('Y-m-d H:i:s');
//echo $result.'<br>';
//echo $offset.' seconds';

//Calculate difference in time with seconds ($offset = seconds)
$tz_seconds = 14400 - $offset; //Mauritius is GMT+4 = 14,400 seconds = 4 hours
$tz_seconds_str = (string)$tz_seconds . " seconds";

$tz_value_str = "";
$tz_Value = $tz_seconds / 3600;

if ($tz_Value == 0) {
        $tz_value_str = "GMT+4";
} else {
        if ($tz_Value > 0) {
                $tz_value_str = "GMT+".$tz_Value;
        } else {
                $tz_value_str = "GMT".$tz_Value;
        }
}

if ($daylight_savings_on == "YES") {

        //Convert to new time of a bank that is abroad
        if ($bankTimezone != "Indian/Mauritius") {
                $time_BANK_timezone_from = date("Y-m-d H:i:s", strtotime($tz_seconds_str, strtotime($time_BANK_timezone_from)));
                $time_BANK_timezone_to = date("Y-m-d H:i:s", strtotime($tz_seconds_str, strtotime($time_BANK_timezone_to)));
        }
}
//end of Calculating timezone with daylight savings
//#######################################################################################
else {
        //Set Time according to the bank timezone
        if ($bankTimezone == "Europe/Riga") {
                $time_BANK_timezone_from = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_from'])));
                $time_BANK_timezone_to = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_to'])));
        }
}

$fromdate_notime = date('Y-m-d', strtotime($assoc_array['fromdate']));
$fromdate_notime_temp = date('Y-m-d', strtotime( '+1 day', strtotime($assoc_array['todate'])));
//$todate_notime = date("Y-m-d", strtotime("-1 second", strtotime($fromdate_notime_temp)));
$todate_notime = $fromdate_notime_temp;

$cb_fromdate_notime = date('Y-m-d', strtotime($assoc_array['fromdate']));
$cb_fromdate_notime_temp = date('Y-m-d', strtotime( '+1 day', strtotime($assoc_array['todate'])));
//$todate_notime = date("Y-m-d", strtotime("-1 second", strtotime($fromdate_notime_temp)));
$cb_todate_notime = $fromdate_notime_temp;

//$time_BANK_timezone_from = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_from'])));
//$time_BANK_timezone_to = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_to'])));


$start_date_of_month = date("Y-m-01 00:00:00", strtotime($assoc_array['fromdate']));
$end_date_of_month = date("Y-m-t 23:59:59", strtotime($start_date_of_month));

$start_date_of_month_str = date("d-M-Y", strtotime($start_date_of_month));
$end_date_of_month_str = date("d-M-Y", strtotime($end_date_of_month));

$start_date_last_month = date('Y-m-d', strtotime('-1 months', strtotime($start_date_of_month)));
$end_date_of_last_month = date("Y-m-t", strtotime($start_date_last_month));

$start_date_last_month_str = date("d-M-Y", strtotime($start_date_last_month));
$end_date_of_last_month_str = date("d-M-Y", strtotime($end_date_of_last_month));

//var_dump($assoc_array);


/************ FROM TRANSACT 24 ************/

//get the number of trns (SALES), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated

//Cn Testing
//$sql_val = "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between ".$time_BANK_timezone_from." and ".$time_BANK_timezone_to." and recon_status = 1 and status_id in (12,13,15,51,57) group by merchant_id";
//$qinsertqueryrow = "INSERT INTO trn_queries (sql_text, env_type) VALUES ('".$sql_val."', 'statement_test')";

//$res_qinsertqueryrow = pg_query($pg_conn, $qinsertqueryrow);
//$row_query_id = pg_fetch_row($res_qinsertqueryrow);

$query_count_number_sales_trn = pg_query($pg_conn, "select t24_merchant_id, count(*), sum(recon_upl_amount) from recon_upl where t24_merchant_id::int = ".$merchantid." and recon_upl_transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and recon_upl_trn_status in (12) group by t24_merchant_id");   
$count_sales_array = pg_fetch_row($query_count_number_sales_trn);

//Sales VISA
$query_count_number_sales_VISA_trn = pg_query($pg_conn, "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '4%' group by merchant_id");
$count_sales_VISA_array = pg_fetch_row($query_count_number_sales_VISA_trn);

//Sales Mastercard
$query_count_number_sales_MC_trn = pg_query($pg_conn, "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '5%' group by merchant_id");   
$count_sales_MC_array = pg_fetch_row($query_count_number_sales_MC_trn);

//get the number of trns (DECLINES), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated
$query_count_number_declined_trn = pg_query($pg_conn, "select status_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and status_id in (5,6,7) group by status_id"); //and recon_status = 1

$count_declined_array = pg_fetch_row($query_count_number_declined_trn);

//get the number of trns (FRAUD), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated
//$query_count_number_fraud_trn = pg_query($pg_conn, "select fraud_status_id, count(*), sum(fraud_amount) from fraud where fraud_initiated_by = ".$merchantid." and fraud_date_received between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and fraud_recon_status = 1 and fraud_status_id in (13) group by fraud_status_id");
$query_count_number_fraud_trn = pg_query($pg_conn, "select fraud_status_id, count(*), sum(fraud_amount) from fraud where fraud_initiated_by = ".$merchantid." and fraud_date_received between '".$time_date_from."' and '".$time_date_to."' and fraud_recon_status = 1 and fraud_status_id in (13) group by fraud_status_id");
$count_fraud_array = pg_fetch_row($query_count_number_fraud_trn);

//Cn Testing
//$sql_val2 = "select fraud_status_id, count(*), sum(fraud_amount) from fraud where fraud_initiated_by = ".$merchantid." and fraud_date_received between ".$time_BANK_timezone_from." and ".$time_BANK_timezone_to." and fraud_recon_status = 1 and fraud_status_id in (13) group by fraud_status_id";
//$qinsertqueryrowf = "INSERT INTO trn_queries (sql_text, env_type) VALUES ('".$sql_val2."', 'statement_test_fraud')";

//$res_qinsertqueryrowf = pg_query($pg_conn, $qinsertqueryrowf);
//$row_query_idf = pg_fetch_row($res_qinsertqueryrowf);

//Fraud VISA
//$query_count_number_fraud_VISA_trn = pg_query($pg_conn, "select f.fraud_status_id, count(*), sum(f.fraud_amount) from fraud f, transaction_ext t where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and f.fraud_recon_status = 1 and f.fraud_status_id in (13) and f.fraud_transaction_id = t.transaction_id and t.cus_ccnumber like '4%' group by fraud_status_id");
$query_count_number_fraud_VISA_trn = pg_query($pg_conn, "select f.fraud_status_id, count(*), sum(f.fraud_amount) from fraud f, transaction_ext t where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$time_date_from."' and '".$time_date_to."' and f.fraud_recon_status = 1 and f.fraud_status_id in (13) and f.fraud_transaction_id = t.transaction_id and t.cus_ccnumber like '4%' group by fraud_status_id");
$count_fraud_VISA_array = pg_fetch_row($query_count_number_fraud_VISA_trn);

//Fraud Mastercard
//$query_count_number_fraud_MC_trn = pg_query($pg_conn, "select f.fraud_status_id, count(*), sum(f.fraud_amount) from fraud f, transaction_ext t where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and f.fraud_recon_status = 1 and f.fraud_status_id in (13) and f.fraud_transaction_id = t.transaction_id and t.cus_ccnumber like '5%' group by fraud_status_id");
$query_count_number_fraud_MC_trn = pg_query($pg_conn, "select f.fraud_status_id, count(*), sum(f.fraud_amount) from fraud f, transaction_ext t where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$time_date_from."' and '".$time_date_to."' and f.fraud_recon_status = 1 and f.fraud_status_id in (13) and f.fraud_transaction_id = t.transaction_id and t.cus_ccnumber like '5%' group by fraud_status_id");
$count_fraud_MC_array = pg_fetch_row($query_count_number_fraud_MC_trn);

//---------------------------------------------------------------------

//Fraud Ratios VISA
//Get the number of frauds for the current month divided by the number of transactions for the current month. 

// -------- VISA Number of transactions for the current month
$qry_fraud_trns_current_month_VISA = pg_query($pg_conn, "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$start_date_of_month."' and '".$end_date_of_month."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '4%' group by merchant_id");   
$count_fraud_trns_current_month_VISA = pg_fetch_row($qry_fraud_trns_current_month_VISA);

// -------- VISA Number of frauds for the current month
$qry_frauds_current_month_VISA = pg_query($pg_conn, "select count(f.fraud_id), sum(f.fraud_amount) from fraud f, transaction_ext tr where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$start_date_of_month."' and '".$end_date_of_month."' and tr.transaction_id = f.fraud_transaction_id and tr.cus_ccnumber like '4%' and tr.status_id in (12,13,15,51,57)");   
$count_frauds_current_month_VISA = pg_fetch_row($qry_frauds_current_month_VISA);

//Fraud Ratios Mastercard
//Get the number of frauds for the current month divided by the number of transactions for the previous month

// -------- Mastercard - Number of transactions for the previous month
$qry_fraud_trns_last_month_MC = pg_query($pg_conn, "select count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$start_date_last_month."' and '".$end_date_of_last_month."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '5%' group by merchant_id");   
$count_fraud_trns_last_month_MC = pg_fetch_row($qry_fraud_trns_last_month_MC);

//echo "TEST###################";
//echo $count_fraud_trns_current_month_VISA[1];

// -------- Mastercard - Number of frauds for the current month
$qry_frauds_current_month_MC = pg_query($pg_conn, "select count(f.fraud_id), sum(f.fraud_amount) from fraud f, transaction_ext tr where f.fraud_initiated_by = ".$merchantid." and f.fraud_date_received between '".$start_date_of_month."' and '".$end_date_of_month."' and tr.transaction_id = f.fraud_transaction_id and tr.cus_ccnumber like '5%' and tr.status_id in (12,13,15,51,57)");   
$count_frauds_current_month_MC = pg_fetch_row($qry_frauds_current_month_MC);

//---------------------------------------------------------------------------

//get the number of trns (REFUNDS) within the date generated
$query_count_number_refund_trn = pg_query($pg_conn, "select recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl where recon_upl_transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and t24_merchant_id::int = ".$merchantid." and recon_upl_trn_status in (15)  group by recon_upl_trn_status");
$count_refund_array = pg_fetch_row($query_count_number_refund_trn);

//Refunds VISA
$query_count_number_refund_VISA_trn = pg_query($pg_conn, "select  r.status_id, count(*), sum(refund_amount) from refund r, transaction_ext t where r.refund_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and r.initiated_by = ".$merchantid." and t.transaction_id = r.transaction_id and r.recon_status = 1 and r.status_id = 15 and t.cus_ccnumber like '4%' group by r.status_id");   
$count_refund_VISA_array = pg_fetch_row($query_count_number_refund_VISA_trn);

//Refunds Mastercard
$query_count_number_refund_MC_trn = pg_query($pg_conn, "select  r.status_id, count(*), sum(refund_amount) from refund r, transaction_ext t where r.refund_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and r.initiated_by = ".$merchantid." and t.transaction_id = r.transaction_id and r.recon_status = 1 and r.status_id = 15 and t.cus_ccnumber like '5%' group by r.status_id");   
$count_refund_MC_array = pg_fetch_row($query_count_number_refund_MC_trn);

//get the number of trns (CHARGEBACKS) within the date generated, minus 4hours and 1 minutes from the to date not to get the chargebacks trns of the next weekly statement. As katherine enters the correct date in hk Payment Gateway interface it saves the date with midnight time hk time. Converting to mauritius time, it automatically removes 4 hours on the database. so to bear this conversion, we also minus the time difference to match with the hk time. and avoid taking cb trns of next weekly statement.

//$query_count_number_chargebacks_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." group by c.status_id");

//Rietumu uses the chargeback allocated_to field for SQL queries
//CIM uses initiated_by field, and not the allocated_to field

if ($merchantid == 2841 || $merchantid == 1601 || $merchantid == 1822 || $merchantid == 2222 || $merchantid == 2021) {
        //CIM
        //$query_count_number_chargebacks_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.initiated_by = ".$merchantid." group by c.status_id");        
        $query_count_number_chargebacks_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.initiated_by = ".$merchantid." and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.chargeback_amount > 0 group by c.status_id");        

        //CB count from 1st of the month to current date
        $query_count_number_chargebacks_trn_per_month = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$cb_fromdate_notime."' and '".$cb_todate_notime."' and c.initiated_by = ".$merchantid." and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.chargeback_amount > 0 group by c.status_id");        
} else {
        //All other banks
        $query_count_number_chargebacks_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and c.chargeback_amount > 0 group by c.status_id");

        //CB count from 1st of the month to current date
        $query_count_number_chargebacks_trn_per_month = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$cb_fromdate_notime."' and '".$cb_todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and c.chargeback_amount > 0 group by c.status_id");
}

//$query_count_number_chargebacks_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." group by c.status_id");
$count_cb_array = pg_fetch_row($query_count_number_chargebacks_trn);

//Chargebacks per month
$count_cb_per_month_array = pg_fetch_row($query_count_number_chargebacks_trn_per_month);

//Chargeback VISA
//$query_count_number_chargebacks_VISA_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.cus_ccnumber like '4%' group by c.status_id");
$query_count_number_chargebacks_VISA_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.cus_ccnumber like '4%' group by c.status_id");
$count_cb_VISA_array = pg_fetch_row($query_count_number_chargebacks_VISA_trn);

//Chargeback Mastercard
//$query_count_number_chargebacks_MC_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.cus_ccnumber like '5%' group by c.status_id");
$query_count_number_chargebacks_MC_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.cus_ccnumber like '5%' group by c.status_id");
$count_cb_MC_array = pg_fetch_row($query_count_number_chargebacks_MC_trn);

//Chargeback Ratios VISA
//Get the number of chargebacks for the current month divided by the number of transactions for the current month. 

// -------- VISA Number of transactions for the current month
$qry_chargeback_trns_current_month_VISA = pg_query($pg_conn, "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$start_date_of_month."' and '".$end_date_of_month."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '4%' group by merchant_id");   
$count_chargeback_trns_current_month_VISA = pg_fetch_row($qry_chargeback_trns_current_month_VISA);

// -------- VISA Number of chargebacks for the current month
$qry_chargeback_current_month_VISA = pg_query($pg_conn, "select count(c.chargeback_id), sum(c.chargeback_amount) from chargeback c, transaction_ext tr where c.allocated_to = ".$merchantid." and c.chargeback_letter_date between '".$start_date_of_month."' and '".$end_date_of_month."' and tr.transaction_id = c.transaction_id and tr.cus_ccnumber like '4%' and tr.status_id in (12,13,15,51,57)");   
$count_chargeback_current_month_VISA = pg_fetch_row($qry_chargeback_current_month_VISA);

//Chargeback Ratios Mastercard
//Get the number of chargebacks for the current month divided by the number of transactions for the previous month

// -------- Mastercard - Number of transactions for the previous month
$qry_chargeback_trns_last_month_MC = pg_query($pg_conn, "select count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$start_date_last_month."' and '".$end_date_of_last_month."' and recon_status = 1 and status_id in (12,13,15,51,57) and cus_ccnumber like '5%' group by merchant_id");   
$count_chargeback_trns_last_month_MC = pg_fetch_row($qry_chargeback_trns_last_month_MC);

// -------- Mastercard - Number of chargebacks for the current month
$qry_chargebacks_current_month_MC = pg_query($pg_conn, "select count(c.chargeback_id), sum(c.chargeback_amount) from chargeback c, transaction_ext tr where c.allocated_to = ".$merchantid." and c.chargeback_letter_date between '".$start_date_of_month."' and '".$end_date_of_month."' and tr.transaction_id = c.transaction_id and tr.cus_ccnumber like '5%' and tr.status_id in (12,13,15,51,57)");   
$count_chargebacks_current_month_MC = pg_fetch_row($qry_chargebacks_current_month_MC);

//Chargeback Reversals (Negative values in Database)
$query_count_number_chargebacks_rev_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(chargeback_amount) from chargeback c, transaction_ext t where c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.transaction_id = c.transaction_id and c.recon_status = 1 and c.status_id = 51 and c.allocated_to = ".$merchantid." and c.chargeback_amount < 0 group by c.status_id");
$count_cb_rev_array = pg_fetch_row($query_count_number_chargebacks_rev_trn);

//Chargebacks Debited with different currency
//Chargebacks that have been debited from a Merchant's account in different currencies
//$query_chargebacks_debited = pg_query($pg_conn, "select transaction_id, chargeback_letter_date from chargeback where initiated_by = ".$merchantid." and allocated_to <> 0 and allocated_to = ".$merchantid." and chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."'");
//$query_chargebacks_debited = pg_query($pg_conn, "select cb.transaction_id, cb.chargeback_letter_date, cb.chargeback_currency, cb.allocated_to,  m.merchant_name from chargeback cb, merchant m where cb.initiated_by = ".$merchantid." and cb.chargeback_letter_date between '".$excel_trn_date_minusfourhours."' and '".$excel_trn_date_minusfourhoursto."' and cb.allocated_to <> 0 and cb.allocated_to <> ".$merchantid." and cb.allocated_to = m.merchant_id order by cb.allocated_to, cb.chargeback_letter_date");

$query_chargebacks_debited = pg_query($pg_conn, "select cb.transaction_id, cb.chargeback_letter_date, cb.chargeback_currency, cb.allocated_to,  m.merchant_name from chargeback cb, merchant m where cb.initiated_by = ".$merchantid." and cb.allocated_to <> 0 and cb.allocated_to <> ".$merchantid." and cb.allocated_to = m.merchant_id and cb.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' order by cb.allocated_to, cb.chargeback_letter_date");
$count_chargebacks_debited = pg_fetch_all($query_chargebacks_debited);

//get the number of trns (REPRESENTMENTS/CHARGEBACK REVERSAL) within the date generated
$query_count_number_chargebacks_reversal_trn = pg_query($pg_conn, "select cbr.cb_reversal_status_id, count(*), sum(cbr.cb_reversal_amount) from chargeback_reversal cbr, transaction_ext t where cbr.cb_reversal_registered_date between '".$fromdate_notime."' and '".$todate_notime."' and cbr.cb_reversal_allocated_to = ".$merchantid." and t.transaction_id = cbr.cb_reversal_transaction_id and cbr.cb_reversal_recon_status = 1 and cbr.cb_reversal_status_id = 57 group by cbr.cb_reversal_status_id");

$count_cb_reversal_array = pg_fetch_row($query_count_number_chargebacks_reversal_trn);

if (!empty($count_sales_array))
{  
        $sales_array['count'] = $count_sales_array[1];
        $sales_array['sum'] = (float)$count_sales_array[2]/100;
}
if (!empty($count_sales_VISA_array))
{  
        $sales_VISA_array['count'] = $count_sales_VISA_array[1];
        $sales_VISA_array['sum'] = (float)$count_sales_VISA_array[2]/100;
}
if (!empty($count_sales_MC_array))
{  
        $sales_MC_array['count'] = $count_sales_MC_array[1];
        $sales_MC_array['sum'] = (float)$count_sales_MC_array[2]/100;
}
if (!empty($count_declined_array))
{  
        $declines_array['count'] = $count_declined_array[1];
        $declines_array['sum'] = (float)$count_declined_array[2]/100;
}
if (!empty($count_refund_array))
{  
        $refunds_array['count'] = $count_refund_array[1];
        $refunds_array['sum'] = (float)$count_refund_array[2]/100;
}
if (!empty($count_refund_VISA_array))
{  
        $refunds_VISA_array['count'] = $count_refund_VISA_array[1];
        $refunds_VISA_array['sum'] = (float)$count_refund_VISA_array[2]/100;
}
if (!empty($count_refund_MC_array))
{  
        $refunds_MC_array['count'] = $count_refund_MC_array[1];
        $refunds_MC_array['sum'] = (float)$count_refund_MC_array[2]/100;
}
if (!empty($count_fraud_array))
{  
        $fraudalers_array['count'] = $count_fraud_array[1];
        $fraudalers_array['sum'] = (float)$count_fraud_array[2]/100;
}
if (!empty($count_fraud_VISA_array))
{  
        $fraudalers_VISA_array['count'] = $count_fraud_VISA_array[1];
        $fraudalers_VISA_array['sum'] = (float)$count_fraud_VISA_array[2]/100;
}
if (!empty($count_fraud_MC_array))
{  
        $fraudalers_MC_array['count'] = $count_fraud_MC_array[1];
        $fraudalers_MC_array['sum'] = (float)$count_fraud_MC_array[2]/100;
}
if (!empty($count_fraud_trns_current_month_VISA))
{  
        $fraud_trns_current_month_VISA_array['count'] = $count_fraud_trns_current_month_VISA[1];
        $fraud_trns_current_month_VISA_array['sum'] = (float)$count_fraud_trns_current_month_VISA[2]/100;
}
if (!empty($count_frauds_current_month_VISA))
{  
        $fraud_current_month_VISA_array['count'] = $count_frauds_current_month_VISA[0];
        $fraud_current_month_VISA_array['sum'] = (float)$count_frauds_current_month_VISA[1]/100;
}

//Calculate Fraud VISA Ratio Percentage
$fr_perc = ($fraud_current_month_VISA_array['sum'] / $fraud_trns_current_month_VISA_array['sum']) * 100;
$fraud_ratio_perc_VISA_value = round($fr_perc, 2);

$fr_perc = ($fraud_current_month_VISA_array['count'] / $fraud_trns_current_month_VISA_array['count']) * 100;
$fraud_ratio_perc_VISA_count = round($fr_perc, 2);

if (!empty($count_fraud_trns_last_month_MC))
{  
        $fraud_trns_last_month_MC_array['count'] = $count_fraud_trns_last_month_MC[0];
        $fraud_trns_last_month_MC_array['sum'] = (float)$count_fraud_trns_last_month_MC[1]/100;
}
if (!empty($count_frauds_current_month_MC))
{  
        $fraud_current_month_MC_array['count'] = $count_frauds_current_month_MC[0];
        $fraud_current_month_MC_array['sum'] = (float)$count_frauds_current_month_MC[1]/100;
}

//Calculate Fraud Mastercard Ratio Percentage
$fr_perc = ($fraud_current_month_MC_array['sum'] / $fraud_trns_last_month_MC_array['sum']) * 100;
$fraud_ratio_perc_MC_value = round($fr_perc,2);

$fr_perc = ($fraud_current_month_MC_array['count'] / $fraud_trns_last_month_MC_array['count']) * 100;
$fraud_ratio_perc_MC_count = round($fr_perc,2);

if (!empty($count_cb_array))
{  
        $chargebacks_array['count'] = $count_cb_array[1];
        $chargebacks_array['sum'] = (float)$count_cb_array[2]/100;
}
if (!empty($count_cb_per_month_array))
{  
        $chargebacks_per_month_array['count'] = $count_cb_per_month_array[1];
        $chargebacks_per_month_array['sum'] = (float)$count_cb_per_month_array[2]/100;
}
if (!empty($count_cb_VISA_array))
{  
        $chargebacks_VISA_array['count'] = $count_cb_VISA_array[1];
        $chargebacks_VISA_array['sum'] = (float)$count_cb_VISA_array[2]/100;
}
if (!empty($count_cb_MC_array))
{  
        $chargebacks_MC_array['count'] = $count_cb_MC_array[1];
        $chargebacks_MC_array['sum'] = (float)$count_cb_MC_array[2]/100;
}
//Representments
if (!empty($count_cb_reversal_array))
{
        $representments_array['count'] = $count_cb_reversal_array[1];
        $representments_array['sum'] = (float)$count_cb_reversal_array[2]/100;
}
//Chargeback Reversals (Negative values in Database)
if (!empty($count_cb_rev_array))
{  
        $chargeback_reversals_array['count'] = $count_cb_rev_array[1];
        $chargeback_reversals_array['sum'] = abs((float)$count_cb_rev_array[2]/100);
}

if (!empty($count_chargeback_trns_current_month_VISA)) {
        $chargebacks_trns_current_month_VISA_array['count'] = $count_chargeback_trns_current_month_VISA[1];
        $chargebacks_trns_current_month_VISA_array['sum'] = (float)$count_chargeback_trns_current_month_VISA[2]/100;
}
if(!empty($count_chargeback_current_month_VISA)) {
        $chargebacks_current_month_VISA_array['count'] = $count_chargeback_current_month_VISA[0];
        $chargebacks_current_month_VISA_array['sum'] = (float)$count_chargeback_current_month_VISA[1]/100;
}

//Calculate Chargebacks VISA Ratio Percentage
$cb_perc = ($chargebacks_current_month_VISA_array['sum'] / $chargebacks_trns_current_month_VISA_array['sum']) * 100;
$chargeback_ratio_perc_VISA_value = round($cb_perc, 2);

$cb_perc = ($chargebacks_current_month_VISA_array['count'] / $chargebacks_trns_current_month_VISA_array['count']) * 100;
$chargeback_ratio_perc_VISA_count = round($cb_perc, 2);

if (!empty($count_chargeback_trns_last_month_MC)) {
        $chargebacks_trns_last_month_MC_array['count'] = $count_chargeback_trns_last_month_MC[0];
        $chargebacks_trns_last_month_MC_array['sum'] = (float)$count_chargeback_trns_last_month_MC[1]/100;
}
if (!empty($count_chargebacks_current_month_MC)) {
        $chargebacks_current_month_MC_array['count'] = $count_chargebacks_current_month_MC[0];
        $chargebacks_current_month_MC_array['sum'] = (float)$count_chargebacks_current_month_MC[1]/100;
}

//Calculate Chargebacks Mastercard Ratio Percentage
$cb_perc = ($chargebacks_current_month_MC_array['sum'] / $chargebacks_trns_last_month_MC_array['sum']) * 100;
$chargeback_ratio_perc_MC_value = round($cb_perc, 2);

$cb_perc = ($chargebacks_current_month_MC_array['count'] / $chargebacks_trns_last_month_MC_array['count']) * 100;
$chargeback_ratio_perc_MC_count = round($cb_perc, 2);

if (!empty($count_chargebacks_debited)) {
        $chargebacks_debited_array['transaction_id'] = $count_chargebacks_debited['transaction_id'];
        $chargebacks_debited_array['chargeback_letter_date'] = $count_chargebacks_debited['chargeback_letter_date'];
        $chargebacks_debited_array['merchant_name'] = $count_chargebacks_debited['merchant_name'];
}

//Negative Adjustments
$neg_adj_items_str = "";
if ($adj_items < 0) {
        $neg_adj_items_str = 'color:red;">-';
} else {
        $neg_adj_items_str = '">';
}
$neg_adj_auth_amt_str = "";
if ($adj_auth_amt < 0) {
        $neg_adj_auth_amt_str = 'color:red;">';
} else {
        $neg_adj_auth_amt_str = '">';
}
$neg_adj_settle_amt_str = "";
if ($adj_settle_amt < 0) {
        $neg_adj_settle_amt_str = 'color:red;">';
} else {
        $neg_adj_settle_amt_str = '">';
}

//Negative Chargeback sum values
$neg_chargeback_sum_str = "";
if ($chargebacks_array['sum'] > 0) {
        $neg_chargeback_sum_str = 'color:red;">-';
} else {
        $neg_chargeback_sum_str = '">';
}

//Add adjustments data here
//$adjustments_array['status_id'] = $array['status_id'];
//$adjustments_array['count'] = $array['count'];
//$adjustments_array['sum'] = (float)$array['sum']/100;



$rolling_reserve_time_from = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_from'])));
$rolling_reserve_time_to = date("Y-m-d H:i:s", strtotime("+120 minutes", strtotime($assoc_array['sel_date_to'])));


$excel_trn_date_minus180 = date("Y-m-d H:i:s", strtotime ( '-180 day' , strtotime($rolling_reserve_time_from)));
$excel_trn_date_minus180to = date("Y-m-d H:i:s", strtotime ( '-180 day' , strtotime($rolling_reserve_time_to)));

//echo $excel_trn_date_minus180;
//echo $excel_trn_date_minus180to;


/******* FROM BANK EXCEL SHEET ********/

//get the number of trns (SALES), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated
//$query_count_number_trn_recon = pg_query($pg_conn, "select merchant_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and recon_status = 1 and status_id in (12,13,15,51,57) group by merchant_id");
$query_count_number_trn_recon = pg_query($pg_conn, "select t24_merchant_id, count(*), sum(recon_upl_amount) from recon_upl where t24_merchant_id::int = ".$merchantid." and recon_upl_transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and recon_upl_trn_status in (12) group by t24_merchant_id");
$count_recon_array = pg_fetch_row($query_count_number_trn_recon);

//get the number of trns (DECLINES), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated
//$query_count_number_trn_decline_recon = pg_query($pg_conn, "select r.recon_upl_trn_status as status_id, count(*), sum(r.recon_upl_amount) from recon_upl r, transaction_ext t where t.transaction_id = r.recon_upl_transaction_id::numeric and t.recon_status = 1 and t.transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and t.merchant_id = ".$merchantid." and r.recon_upl_trn_status in (5) group by r.recon_upl_trn_status");
$query_count_number_trn_decline_recon = pg_query($pg_conn, "select status_id, count(*), sum(trans_amount) from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and status_id in (5,6,7) group by status_id");
$count_recon_decline_array = pg_fetch_row($query_count_number_trn_decline_recon);

//get the number of trns (FRAUDS) within the date generated
//$query_count_number_fraud_recon_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 group by r.recon_upl_trn_status");   
$query_count_number_fraud_recon_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$time_date_from."' and '".$time_date_to."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 group by r.recon_upl_trn_status");   
$count_fraud_recon_array = pg_fetch_row($query_count_number_fraud_recon_trn);

//Fraud VISA
//$query_count_number_fraud_recon_VISA_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f, transaction_ext t where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 and t.transaction_id::text = r.recon_upl_transaction_id
$query_count_number_fraud_recon_VISA_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f, transaction_ext t where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$time_date_from."' and '".$time_date_to."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 and t.transaction_id::text = r.recon_upl_transaction_id
and t.cus_ccnumber like '4%' group by r.recon_upl_trn_status");   
$count_fraud_recon_VISA_array = pg_fetch_row($query_count_number_fraud_recon_VISA_trn);

//Fraud Mastercard
//$query_count_number_fraud_recon_MC_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f, transaction_ext t where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 and t.transaction_id::text = r.recon_upl_transaction_id
$query_count_number_fraud_recon_MC_trn = pg_query($pg_conn, "select r.recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl r, fraud f, transaction_ext t where f.fraud_recon_upl_id = r.recon_upl_id and f.fraud_transaction_id::text = r.recon_upl_transaction_id and f.fraud_recon_status = 1 and r.recon_upl_transaction_date between '".$time_date_from."' and '".$time_date_to."' and f.fraud_initiated_by =  ".$merchantid." and r.recon_upl_trn_status = 13 and t.transaction_id::text = r.recon_upl_transaction_id
and t.cus_ccnumber like '5%' group by r.recon_upl_trn_status");   
$count_fraud_recon_MC_array = pg_fetch_row($query_count_number_fraud_recon_MC_trn);

//get the number of trns (REFUNDS) within the date generated
$query_count_number_refund_recon_trn = pg_query($pg_conn, "select recon_upl_trn_status, count(*), sum(recon_upl_amount) from recon_upl where recon_upl_transaction_date between '".$time_BANK_timezone_from."' and '".$time_BANK_timezone_to."' and t24_merchant_id::int = ".$merchantid." and recon_upl_trn_status in (15)  group by recon_upl_trn_status");   
$count_refund_recon_array = pg_fetch_row($query_count_number_refund_recon_trn);


//get the number of trns (CHARGEBACKS) within the date generated, minus 4hours and 1 minutes from the to date not to get the chargebacks trns of the next weekly statement. As katherine enters the correct date in hk Payment Gateway interface it saves the date with midnight time hk time. Converting to mauritius time, it automatically removes 4 hours on the database. so to bear this conversion, we also minus the time difference to match with the hk time. and avoid taking cb trns of next weekly statement.
//$query_count_number_chargebacks_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and c.status_id = 51 and c.allocated_to = ".$merchantid." group by c.status_id");

//Rietumu uses the chargeback allocated_to field for SQL queries
//CIM uses initiated_by field, and not the allocated_to field

//Cn Testing
//$sql_val = "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between ".$fromdate_notime." and ".$todate_notime." and c.status_id = 51 and c.initiated_by = ".$merchantid." group by c.status_id";
//$qinsertqueryrow = "INSERT INTO trn_queries (sql_text, env_type) VALUES ('".$sql_val."', 'statement_test')";
//$res_qinsertqueryrow = pg_query($pg_conn, $qinsertqueryrow);
//$row_query_id = pg_fetch_row($res_qinsertqueryrow);

if ($merchantid == 2841 || $merchantid == 1601) {
        //CIM
        //$query_count_number_chargebacks_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.initiated_by = ".$merchantid." group by c.status_id");

        $query_count_number_chargebacks_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, transaction_ext t, chargeback c where t.recon_upl_id = r.recon_upl_id and t.transaction_id =  c.transaction_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and t.merchant_id = ".$merchantid." and c.status_id = 51 and c.chargeback_amount > 0 group by c.status_id");

} else {
        //All other banks
        $query_count_number_chargebacks_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and c.chargeback_amount > 0 group by c.status_id");
}

//$query_count_number_chargebacks_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.allocated_to = ".$merchantid." group by c.status_id");
$count_chargeback_recon_array = pg_fetch_row($query_count_number_chargebacks_recon_trn);

//Chargeback VISA
//$query_count_number_chargebacks_recon_VISA_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c, transaction_ext t where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.transaction_id::text = r.recon_upl_transaction_id
$query_count_number_chargebacks_recon_VISA_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c, transaction_ext t where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.transaction_id::text = r.recon_upl_transaction_id
and t.cus_ccnumber like '4%' group by c.status_id");
$count_chargeback_recon_VISA_array = pg_fetch_row($query_count_number_chargebacks_recon_VISA_trn);

//Chargeback Mastercard
//$query_count_number_chargebacks_recon_MC_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c, transaction_ext t where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.transaction_id::text = r.recon_upl_transaction_id
$query_count_number_chargebacks_recon_MC_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c, transaction_ext t where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and t.transaction_id::text = r.recon_upl_transaction_id
and t.cus_ccnumber like '5%' group by c.status_id");
$count_chargeback_recon_MC_array = pg_fetch_row($query_count_number_chargebacks_recon_MC_trn);

//get the number of trns (REPRESENTMENTS) within the date generated
//$query_count_number_chargebacks_reversal_recon_trn = pg_query($pg_conn, "select cbr.cb_reversal_status_id, count(*), sum(recon_upl_amount) from recon_upl r, transaction_ext t, chargeback_reversal cbr where t.recon_upl_id = r.recon_upl_id and t.transaction_id = cbr.cb_reversal_transaction_id and cbr.cb_reversal_recon_status = 1 and cbr.cb_reversal_registered_date between '".$assoc_array['sel_date_from']."' and '".$assoc_array['sel_date_to']."' and cbr.cb_reversal_allocated_to = ".$merchantid." and cbr.cb_reversal_status_id = 57 group by cbr.cb_reversal_status_id");  
$query_count_number_chargebacks_reversal_recon_trn = pg_query($pg_conn, "select cbr.cb_reversal_status_id, count(*), sum(recon_upl_amount) from recon_upl r, transaction_ext t, chargeback_reversal cbr where t.recon_upl_id = r.recon_upl_id and t.transaction_id = cbr.cb_reversal_transaction_id and cbr.cb_reversal_recon_status = 1 and cbr.cb_reversal_registered_date between '".$fromdate_notime."' and '".$todate_notime."' and cbr.cb_reversal_allocated_to = ".$merchantid." and cbr.cb_reversal_status_id = 57 group by cbr.cb_reversal_status_id");  
$count_chargeback_reversal_recon_array = pg_fetch_row($query_count_number_chargebacks_reversal_recon_trn);

//Chargeback Reversals (Negative values in Database)
$query_count_number_chargebacks_rev_recon_trn = pg_query($pg_conn, "select c.status_id, count(*), sum(recon_upl_amount) from recon_upl r, chargeback c where c.recon_upl_id = r.recon_upl_id and c.recon_status = 1 and c.chargeback_letter_date between '".$fromdate_notime."' and '".$todate_notime."' and c.status_id = 51 and c.allocated_to = ".$merchantid." and c.chargeback_amount < 0 group by c.status_id");
$count_chargeback_rev_recon_array = pg_fetch_row($query_count_number_chargebacks_rev_recon_trn);



if (!empty($count_recon_array))
{
        $sales_array['recon_count'] = $count_recon_array[1];
        $sales_array['recon_sum'] = (float)$count_recon_array[2]/100;
}
if (!empty($count_recon_decline_array))
{
        $declines_array['recon_count'] = $count_recon_decline_array[1];
        $declines_array['recon_sum'] = (float)$count_recon_decline_array[2]/100;
}
if (!empty($count_fraud_recon_array))
{
        $fraudalers_array['recon_count'] = $count_fraud_recon_array[1];
        $fraudalers_array['recon_sum'] = (float)$count_fraud_recon_array[2]/100;
}
if (!empty($count_fraud_recon_VISA_array))
{
        $fraudalers_VISA_array['recon_count'] = $count_fraud_recon_VISA_array[1];
        $fraudalers_VISA_array['recon_sum'] = (float)$count_fraud_recon_VISA_array[2]/100;
}
if (!empty($count_fraud_recon_MC_array))
{
        $fraudalers_MC_array['recon_count'] = $count_fraud_recon_MC_array[1];
        $fraudalers_MC_array['recon_sum'] = (float)$count_fraud_recon_MC_array[2]/100;
}
if (!empty($count_refund_recon_array))
{  
        $refunds_array['recon_count'] = $count_refund_recon_array[1];
        $refunds_array['recon_sum'] = (float)$count_refund_recon_array[2]/100;
}
if (!empty($count_chargeback_recon_array))
{  
        $chargebacks_array['recon_count'] = $count_chargeback_recon_array[1];
        $chargebacks_array['recon_sum'] = (float)$count_chargeback_recon_array[2]/100;
}
if (!empty($count_chargeback_recon_VISA_array))
{  
        $chargebacks_VISA_array['recon_count'] = $count_chargeback_recon_VISA_array[1];
        $chargebacks_VISA_array['recon_sum'] = (float)$count_chargeback_recon_VISA_array[2]/100;
}
if (!empty($count_chargeback_recon_MC_array))
{  
        $chargebacks_MC_array['recon_count'] = $count_chargeback_recon_MC_array[1];
        $chargebacks_MC_array['recon_sum'] = (float)$count_chargeback_recon_MC_array[2]/100;
}
//Representments
if (!empty($count_chargeback_reversal_recon_array))
{  
        $representments_array['recon_count'] = $count_chargeback_reversal_recon_array[1];
        $representments_array['recon_sum'] = (float)$count_chargeback_reversal_recon_array[2]/100;
}
//Chargeback Reversals (Negative values in Database)
if (!empty($count_chargeback_rev_recon_array))
{  
        $chargeback_reversals_array['recon_count'] = $count_chargeback_rev_recon_array[1];
        $chargeback_reversals_array['recon_sum'] = abs((float)$count_chargeback_rev_recon_array[2]/100);
}

//Negative Totals Refund (Recon) (Transactions)
$neg_refund_recon_totals_str = "";
if ($refunds_array['recon_sum'] == 0) {
        $neg_refund_recon_totals_str = '">'; 
} else {
        $neg_refund_recon_totals_str = 'color:red;">-';
}

//Negative Totals Refund (Transactions)
$neg_refund_trn_totals_str = "";
if ($refunds_array['sum'] == 0) {
        $neg_refund_trn_totals_str = '">'; 
} else {
        $neg_refund_trn_totals_str = 'color:red;">-';
}

//Negative Chargeback sum values
$neg_chargeback_recon_str = "";
if ($chargebacks_array['recon_sum'] > 0) {
        $neg_chargeback_recon_str = 'color:red;">-';
} else {
        $neg_chargeback_recon_str = '">';
}


//MDR calculation of TOTAL value
$assoc_array['mdr_total'] = (  ( (float)$assoc_array['mdr_visa'] *  (float)$sales_array['recon_sum'] )  / 100);

//approved transaction total value
$assoc_array['approved_trn_total'] = (float)$assoc_array['transaction_fee_n3d_secure_approved'] * $sales_array['count'];

//declined transaction total value
$assoc_array['declined_trn_total'] = (float)$assoc_array['transaction_fee_n3d_secure_declined'] * $declines_array['count'];

//refund transaction total value
$assoc_array['refund_trn_total'] = (float)$assoc_array['refund_fee'] * $refunds_array['count'];

//fraud alert transaction total value
$assoc_array['fraudalert_trn_total'] = (float)$assoc_array['fraud_alert_fee'] * $fraudalers_array['count'];

if ($chargebacks_per_month_array['count'] >= 100)
{
        //chargebacks < 101/month transaction total value
        $assoc_array['chargebacks_trn_total'] = 0;
        //chargebacks > 101/month transaction total value
        $assoc_array['chargebacks2_trn_total'] = (float)$assoc_array['chargeback_fee_more_1_percent'] * $chargebacks_array['count'];
}
else
{
        //chargebacks < 101/month transaction total value
        $assoc_array['chargebacks_trn_total'] = (float)$assoc_array['chargeback_fee'] * $chargebacks_array['count'];
        //chargebacks > 101/month transaction total value
        $assoc_array['chargebacks2_trn_total'] = 0;
}

//representment fees calculation
$assoc_array['representment_total'] = (float)$assoc_array['chargeback_representment_fee'] * $representments_array['count'];


//Calculate Fees Total
$chb_total = 0;
if ($chargebacks_per_month_array['count'] > 100) {
    $chb_total = $assoc_array['chargebacks2_trn_total'];
} else {
        $chb_total = $assoc_array['chargebacks_trn_total'];
}
 

$fees_total = $assoc_array['mdr_total'] + $assoc_array['approved_trn_total'] + $assoc_array['declined_trn_total'] + 
                $assoc_array['refund_trn_total'] + $assoc_array['fraudalert_trn_total'] + $chb_total +
                $assoc_array['representment_total'] + $assoc_array['wire_transfer_fee'] + $assoc_array['monthly_statement_fee'];

//Negative Fees Total
$neg_fees_total_str = "";
if ($fees_total == 0) {
        $neg_fees_total_str = '">';
} else {
        $neg_fees_total_str = 'color:red;">-';
}

//Negative Merchant Discount Rate
$neg_merchant_disc_rate_str = "";
if ($assoc_array['mdr_total'] == 0) {
        $neg_merchant_disc_rate_str = '">';
} else {
        $neg_merchant_disc_rate_str = 'color:red;">-';
}

//Negative Approved Transactions
$neg_approved_trns_fees_str = "";
if ($assoc_array['approved_trn_total'] == 0) {
        $neg_approved_trns_fees_str = '">';
} else {
        $neg_approved_trns_fees_str = 'color:red;">-';
}

//Negative Declined Transactions
$neg_declined_trns_fees_str = "";
if ($assoc_array['declined_trn_total'] == 0) {
        $neg_declined_trns_fees_str = '">';
} else {
        $neg_declined_trns_fees_str = 'color:red;">-';
}

//Negative Refund Transactions
$neg_refund_trns_fees_str = "";
if ($assoc_array['refund_trn_total'] == 0) {
        $neg_refund_trns_fees_str = '">';
} else {
        $neg_refund_trns_fees_str = 'color:red;">-';
}

//Negative Fraud Alerts < 100 (Dispute Fees)
$neg_fraud_alert_disp_fees_str = "";
if ($assoc_array['fraudalert_trn_total'] == 0) {
        $neg_fraud_alert_disp_fees_str = '">';
} else {
        $neg_fraud_alert_disp_fees_str = 'color:red;">-';
}

//Negative Chargeback < 100 (Dispute Fees)
$neg_chargebacks_disp_fees_str = "";
if ($assoc_array['chargebacks_trn_total'] == 0) {
        $neg_chargebacks_disp_fees_str = '">';
} else {
        $neg_chargebacks_disp_fees_str = 'color:red;">-';
}

//Negative Chargebacks > 100 (Disputes Fees)
$neg_chargebacks_disp_fees_str2 = "";
if ($assoc_array['chargebacks2_trn_total'] == 0) {
        $neg_chargebacks_disp_fees_str2 = '">';
} else {
        $neg_chargebacks_disp_fees_str2 = 'color:red;">-';
}

//Negative Chargeback representment
$neg_chargeback_rep_str = "";
if ($assoc_array['representment_total'] > 0) {
        $neg_chargeback_rep_str = 'color:red;">-';
} else {
        $neg_chargeback_rep_str = '">';
}

//Negative Wire Transfer Fee
$neg_wire_transfer_fee_str = "";
if ($assoc_array['wire_transfer_fee'] == 0) {
        $neg_wire_transfer_fee_str = '">';
} else {
        $neg_wire_transfer_fee_str = 'color:red;">-';
}

//Negative Monthly Statement Fee
$neg_monthly_statement_fee_str = "";
if ($assoc_array['monthly_statement_fee'] == 0) {
        $neg_monthly_statement_fee_str = '">';
} else {
        $neg_monthly_statement_fee_str = 'color:red;">-';
}

//Negative Rolling Reserve Held
$neg_rolling_reserve_held_str = "";
if ($assoc_array['rolling_reserve_total'] == 0) {
        $neg_rolling_reserve_held_str = '">';
} else {
        $neg_rolling_reserve_held_str = 'color:red;">-';
}

//Negative Rolling Reserve Released
$neg_rolling_reserve_released_str = "";
if ($assoc_array['rolling_reserve_to_release'] == 0) {
        $neg_rolling_reserve_released_str = '">';
} else {
        $neg_rolling_reserve_released_str = 'color:red;">-';
}


$datenow = date('Y-M-d');
$datetimenow = date('Y-M-d H:i:s');
$yearnow = date('Y');








/***************************************************************************************************/
/*************   PART 1: Insert into Statement if confirmed for saving   ***************************/
/*************   PART 2: Insert into rolling reserve daily amount rr held   ************************/
/***************************************************************************************************/


//PART 2
        //get the number of trns (SALES), total amount per distinct status for this merchant that is already reconciled (recon_status = 1) within the date generated
        $query_count_number_sales_per_day = pg_query($pg_conn, "select transaction_date as transaction_date, sum(trans_amount) as trans_amount from transaction_ext where merchant_id = ".$merchantid." and transaction_date between '".$excel_trn_date_minus180."' and '".$excel_trn_date_minus180to."' and status_id in (12,13,15,18,51,57) group by transaction_date order by transaction_date");   

        $count_sales_per_day_array = pg_fetch_all($query_count_number_sales_per_day);
        if (!empty($count_sales_per_day_array))
        {
                foreach ($count_sales_per_day_array as $sales_per_day_array)
                {
                       
                        $rr_processing_date = $sales_per_day_array['transaction_date'];
                        $sales_volume = (float)$sales_per_day_array['trans_amount']/100;
                        $rr_amount_held_no_acc = (((float)$assoc_array['rolling_reserve_amt'] * (float)$sales_volume)/100);
						$rr_amount_held = round($rr_amount_held_no_acc, 3);
						
                        $rr_release_date = date("Y-m-d", strtotime ( '+182 day' , strtotime($rr_processing_date)));
                        //check if exist

                        $q_checkifrrexist = "SELECT exists(select * from rolling_reserve_test where rr_processing_date = '".$rr_processing_date."' and rr_merchant_id = ".$merchantid." and rr_active = 1)";
                        $res_q_checkifrrexist = pg_query($pg_conn, $q_checkifrrexist);
                        $row_q_checkifrrexist = pg_fetch_row($res_q_checkifrrexist);

                        //if exist update else insert values in rollingreserve table
                        if ($row_q_checkifrrexist[0] == "t")
                        {
                                $qupdaterolling = "UPDATE rolling_reserve_test SET rr_active = 1, rr_sales_volume= ".$sales_volume.", rr_amount_held= ".$rr_amount_held.", rr_release_date= '".$rr_release_date."' WHERE rr_processing_date = '".$rr_processing_date."' and rr_merchant_id= ".$merchantid." and rr_active= 1";

                                $res_qupdaterolling = pg_query($pg_conn, $qupdaterolling);  
                                //$row_updaterolling = pg_fetch_row($res_qupdaterolling);
                                //$rr_id = $row_updaterolling['0'];
                                //echo "recon upl id is: ".$recon_upl_id."<br/>";

                                if (!$res_qupdaterolling)
                                {
                                        echo "An error occurred in rolling reserve update action"; exit;
                                }
                        }
                        else
                        {
                                $qinsertrolling = "INSERT INTO rolling_reserve_test(rr_sales_volume, rr_amount_held, rr_processing_date, rr_release_date, rr_merchant_id, rr_active) VALUES ( ".$sales_volume.", ".$rr_amount_held.", '".$rr_processing_date."', '".$rr_release_date."', ".$merchantid.", 1)";
                                //echo $qinsertrow;

                                $res_qinsertrolling = pg_query($pg_conn, $qinsertrolling);  
                                //$res_insertrolling = pg_fetch_row($res_qinsertrolling);
                                //$rr_id = $res_insertrolling['0'];

                                if (!$res_qinsertrolling)
                                {
                                        echo "An error occurred in rolling reserve insert action"; exit;
                                }
                        }
                }
        }
		
		
//get the rolling release amount for this period 
$query_get_rolling_reserve_release = pg_query($pg_conn, "select sum(rr_amount_held) as rr_amount_held from rolling_reserve_test where rr_processing_date between '".$excel_trn_date_minus180."' and '".$excel_trn_date_minus180to."' and rr_merchant_id = ".$merchantid." and rr_active = 1");
$res_query_get_rolling_reserve_release_array = pg_fetch_row($query_get_rolling_reserve_release);


//initialize rolling_reserve_to_release to zero
$assoc_array['rolling_reserve_to_release'] = 0;
if (!empty($res_query_get_rolling_reserve_release_array))
{
        $assoc_array['rolling_reserve_to_release'] = $res_query_get_rolling_reserve_release_array[0];
}


//rolling reserve retained amount calculation
$assoc_array['rolling_reserve_total'] = (((float)$assoc_array['rolling_reserve_amt'] * (float)$sales_array['recon_sum'])/100);

//total due for remittance calculation
$assoc_array['total_due_remittance'] = (float)$sales_array['recon_sum'] + (float)$assoc_array['statement_bbf_amount'] + (float)$representments_array['recon_sum'] + (float)$assoc_array['rolling_reserve_to_release'] - (float)$assoc_array['mdr_total'] - (float)$assoc_array['approved_trn_total'] -  (float)$assoc_array['declined_trn_total'] - (float)$assoc_array['refund_trn_total'] - (float)$assoc_array['fraudalert_trn_total'] - (float)$assoc_array['chargebacks_trn_total'] - (float)$assoc_array['chargebacks2_trn_total'] - (float)$assoc_array['rolling_reserve_total'] - (float)$assoc_array['monthly_statement_fee'] - (float)$assoc_array['wire_transfer_fee'] - (float)$refunds_array['recon_sum'] - (float)$chargebacks_array['sum'] - (float)$assoc_array['representment_total'] + (float)$adj_settle_amt + (float)$chargeback_reversals_array['recon_sum']; //add rolling reserve release that is missing in the total due remittance amount


$assoc_array['statement_bbf_new_amount'] = 0;
//if ($assoc_array['total_due_remittance'] < 1000)
//{
        $assoc_array['statement_bbf_new_amount'] = $assoc_array['total_due_remittance'];
//}

//Negative Total Due Remittance
$neg_tot_due_rem_str = "";
if ($assoc_array['total_due_remittance'] < 0) {
        $neg_tot_due_rem_str = 'color:red;">';
} else {
        $neg_tot_due_rem_str = '">';
}

//Negative balance brought forward
$neg_bal_brought_fwd_str = "";
if ($assoc_array['statement_bbf_amount'] < 0) {
        $neg_bal_brought_fwd_str = 'color:red;">';
} else {
        $neg_bal_brought_fwd_str = '">';
}


//********************* settlement summary section **************************
		
		$statement_number_sett = $yearnow.'/'.$assoc_array['statement_no'].'/'.$assoc_array['merchant_mapping_ref_code'];
		$bbf_sett = number_format($assoc_array['statement_bbf_amount'],2);
		$payment_due_sett = number_format($assoc_array['total_due_remittance'],2);
		$sales_count_sett = $sales_array['recon_count'];
		$sales_volume_sett = number_format($sales_array['recon_sum'],2);
		$decline_count_sett = $declines_array['recon_count'];
		$decline_volume_sett = number_format($declines_array['recon_sum'],2);
		$refund_count_sett = $refunds_array['count'];
		$refund_volume_sett = number_format($refunds_array['recon_sum'],2);
		$cb_count_sett = $chargebacks_array['count'];
		$cb_volume_sett = $chargebacks_array['sum'];
		$sec_count_sett = $representments_array['count'];
		$sec_volume_sett = number_format($representments_array['recon_sum'],2);
		$fraud_count_sett = $fraudalers_array['count'];
		$fraud_volume_sett = number_format($fraudalers_array['recon_sum'],2);
		$cost_sett = number_format($fees_total,2);
		$rolling_reserve_sett = $assoc_array['rolling_reserve_total'];
		$rolling_reserve_release_sett = number_format($assoc_array['rolling_reserve_to_release'],2);
		$period_from_sett = $assoc_array['fromdate'];
		$period_to_sett = $assoc_array['todate'];
		$currency_sett = substr($assoc_array['settlement_currency'], -3);



if ($confirmstatementbox == "confirmstatementbox" )
{
        //PART 1
        $userID = get_current_user_id(); //added to get userid from wordpress

        $end_date_statement = $assoc_array['todate']." 23:59:59";
        //save all necessary data into statement table.
        $qinsertstatementrow = "INSERT INTO statement (statement_merchant_mapping_id, statement_no, statement_user_id, statement_datetime, statement_remit_amount, statement_bbf_amount, statement_paid_to_merchant, statement_datefrom, statement_dateto, statement_active) VALUES (".$assoc_array['merchant_mapping_id'].",".$assoc_array['statement_no'].", ".$userID.",'".$datetimenow."',".$assoc_array['total_due_remittance'].", ".$assoc_array['statement_bbf_new_amount'].", 0,'".$assoc_array['sel_date_from']."','".$end_date_statement."', 1) RETURNING statement_id;";
        
        //echo $qinsertstatementrow;

        $res_qinsertstatementrow = pg_query($pg_conn, $qinsertstatementrow);  
        $row_statement_id = pg_fetch_row($res_qinsertstatementrow);

        $new_statement_id = $row_statement_id['0'];

        if (!$res_qinsertstatementrow)
        {
                echo "An error occurred. See statement insertion section";
        }

        if ($adj_items != "" || $adj_auth_amt != "" || $adj_settle_amt != "" || $adj_note != "")
        {
                //Insert into Adjustments table
                $qinsertadjustmentsrow = "INSERT INTO adjustments (adj_items,adj_authorisation_amt,adj_settlement_amt,adj_note,statement_no,statement_from_date,statement_to_date,merchant_id) VALUES (".$adj_items.", ".$adj_auth_amt.", ".$adj_settle_amt.", '".$adj_note."',' ".$assoc_array['statement_no']."', '".$assoc_array['sel_date_from']."', '".$end_date_statement."', ".$assoc_array['merchant_id'].") ";

                $res_qinsertadjustmentsrow = pg_query($pg_conn, $qinsertadjustmentsrow);  
                $row_adjustment = pg_fetch_row($res_qinsertadjustmentsrow);

                if (!$res_qinsertadjustmentsrow)
                {
                        echo "An error occurred. See adjustments insertion section";
                }
        }
		
		
		
		 $qcheckifuplexist_sett = "SELECT exists( select * from settlement_summary where period_from = '".$period_from_sett."'and merchant_id = '".$merchantid."')";
        
        //echo $qcheckifuplexist;
    
        $res_qcheckifuplexistw_sett = pg_query($pg_conn, $qcheckifuplexist_sett);
        $row_checkifuplexist_sett = pg_fetch_row($res_qcheckifuplexistw_sett);

		 if ($row_checkifuplexist_sett[0] == "f")
        {
		
  //save all necessary data into statement table.
        $qinsertsettlementstatementrow = "INSERT INTO settlement_summary (period_to, period_from, currency, invoice_number,
		bbf, payment_due, sales_count, sales_volume, decline_count, decline_volume,refund_count,refund_volume,cb_count,cb_volume,sec_count,sec_volume,
		fraud_count,fraud_volume,cost,rr,rr_release,settlement_due,settlement_due_last,final_settlement_due,merchant_id)
		VALUES ('".$period_to_sett."','".$period_from_sett."','".$currency_sett."','".$statement_number_sett."','".$bbf_sett."','".$payment_due_sett."','".$sales_count_sett."',
		'".$sales_volume_sett."','".$decline_count_sett."','".$decline_volume_sett."','".$refund_count_sett."','".$refund_volume_sett."','".$cb_count_sett."','".$cb_volume_sett."', '".$sec_count_sett."','".$sec_volume_sett."','".$fraud_count_sett."','".$fraud_volume_sett."','".$cost_sett."','".$rolling_reserve_sett."','".$rolling_reserve_release_sett."','".$payment_due_sett."','".$bbf_sett."','".$payment_due_sett."','".$merchantid."');";
        
        //echo $qinsertstatementrow;

        $res_qinsertsettlementstatementrow = pg_query($pg_conn, $qinsertsettlementstatementrow);  	
		
		}
		
		
		 if ($row_checkifuplexist_sett[0] == "t")
        {
		
  //save all necessary data into statement table.
        $qupdatesettlementstatementrow = "update settlement_summary set period_to = '".$period_to_sett."', period_from = '".$period_from_sett."', currency = '".$currency_sett."', invoice_number = '".$statement_number_sett."',
		bbf = '".$bbf_sett."', payment_due = '".$payment_due_sett."', sales_count = '".$sales_count_sett."', sales_volume = '".$sales_volume_sett."', decline_count = '".$decline_count_sett."', decline_volume = '".$decline_volume_sett."',refund_count = '".$refund_count_sett."',refund_volume = '".$refund_volume_sett."',cb_count = '".$cb_count_sett."',cb_volume = '".$cb_volume_sett."',sec_count = '".$sec_count_sett."',sec_volume = '".$sec_volume_sett."',
		fraud_count = '".$fraud_count_sett."',fraud_volume = '".$fraud_volume_sett."',cost = '".$cost_sett."',rr = '".$rolling_reserve_sett."',rr_release = '".$rolling_reserve_release_sett."',settlement_due = '".$payment_due_sett."',settlement_due_last = '".$bbf_sett."',final_settlement_due = '".$payment_due_sett."',merchant_id = '".$merchantid."' where period_from = '".$period_from_sett."'and merchant_id = '".$merchantid."';";
	

        $res_qupdatesettlementstatementrow = pg_query($pg_conn, $qupdatesettlementstatementrow);  	
		
		
		
		
		//echo $qupdatesettlementstatementrow;
		}
		
		
		
      
}





$page_counter = 1;

$page_counter_footer = set_page_counter_footer($page_counter);

function set_page_counter_footer($page_counter_val) {
        $page_all_counter = 3;
        $footer_val = '
        <table style="width:730px;"><tr>
        <td style="width:92%;"></td>
        <td style="width:8%;text-align:right;"><div style="width:80px;height:30px;background-color:#b5aaaa;">
        <div style="position:relative;text-align:center;margin:0 auto;top:5px;">
        <font style="color:white;font-family:Arial;font-size:10px;">'.$page_counter_val.'/'.$page_all_counter.'
        </font>
        </div>
        </div>
        </td>';
        return $footer_val;
}

//Date for the title headings
$Yr_start = date('Y',strtotime($assoc_array['fromdate']));
$Yr_end = date('Y',strtotime($assoc_array['todate']));
$Mh_start = date('Y',strtotime($assoc_array['fromdate']));
$Mh_end = date('Y',strtotime($assoc_array['todate']));

//Year difference check
$heading_date = "";
if ($Yr_start == $Yr_end) {
        if ($Mh_start == $Mh_end) {
                $heading_date = date('d',strtotime($assoc_array['fromdate'])).' to '.date('d F Y',strtotime($assoc_array['todate']));
        } else {
                $heading_date = date('d F',strtotime($assoc_array['fromdate'])).' to '.date('d F Y',strtotime($assoc_array['todate']));
        }
} else {
        date('d F Y',strtotime($assoc_array['fromdate'])).' to '.date('d F Y',strtotime($assoc_array['todate']));
}

// Set some content to print
$style = '<style>
table.first{background-color: #1e6459;}
th.feesborderbot{border-bottom: 1px solid #82a29c;}
td.feesborderbot{border-bottom: 1px solid #82a29c;}
td.textwhite{color: #fff; font-size:12px;}
td.textwhite{color: #fff; font-size:12px;}
td.textwhiteleft{color: #fff; font-size:8px;text-align:left;width:30%;}
td.textwhiteright{color: #fff; font-size:11px;text-align:right;width:70%;}
td.text-indent: 15px;{text-indent: 15px;}
td.italictext{font-style: italic;font-size: 12px;font-family: Arial;}
td.columnheadings{font-size:15px;font-weight:bold;width:35%;margin-bottom:2px;color:#40a793;}
td.columnheadings_sub{font-size:14px;width:35%;margin-bottom:2px;color:#40a793;}
tr.rowbg{background-color:#82a29c;}
tr.bgrowoddd{background-color:#dcdcdb;}
td.bgrowoddd{background-color:#dcdcdb;}
td.borderbot{border-bottom:1px solid #fff;}
td.borderbot{border-bottom:1px solid #fff;}
th.headertable{color:#44a794; font-size:11px; font-family: Arial;}
td.titleitems{color: #877171;font-size:10px;height:12px;padding:2px;}
tr.titleitems{color: #877171;font-size:10px;height:12px;padding:2px;}
td.note_items{color: #877171;font-size:12px;}
td.note_text{color: #44a794;font-size:12px;}
td.titleitems_bold{color: #877171;font-size:10px;font-weight:bold;}
td.currency_clr{color:#44a794; font-size:8.5px;}
.message_heading{position:relative;left:2px;font-size:14px; color: #82a29c;}
</style>';

echo $style;

/*
$dir_file = "../../upload/*.*";
//$dir_file = get_stylesheet_directory_uri() . '/images/*.*';
//$dir_file = "./converttopdf/template/images/*.*";
echo $dir_file;
echo "<br>";

$files = glob($dir_file);
echo count($files);
for ($i=1; $i<count($files); $i++)
{
    echo $files[$i];
	$num = $files[$i];
	echo '<img src="'.$num.'" alt="random image">'."&nbsp;&nbsp;";
}
*/

//All pages header - Except the Main page
$statement_number = $yearnow.'_'.$assoc_array['statement_no'].'_'.$assoc_array['merchant_mapping_ref_code'];
$val_ref_code = (string)$my_merchant_ref_code;

$statement_number = (string) $statement_number;

$page_header = '<table class="heading_banner" cellpadding="2" cellspacing="0" align="center" style="width:730px;">
<tr>
<td style="position:relative;width:10%;text-align:left;left:10px;" class="headertable pageHeader"><img src="./images/t24.png" height="auto"></td>
<td style="width:50%;text-align:left;" class="headertable pageHeader">
<div>Statement Number '.$yearnow.'/'.$assoc_array['statement_no'].'/'.$assoc_array['merchant_mapping_ref_code'].'<br/>
<span style="font-size:10px;">MID '.$assoc_array['live_mid']." (".$assoc_array['merchant_name'].')</span>
</div>
</td>
<td style="position:relative;width:30%;text-align:right;" class="headertable pageHeader"><div>&nbsp;<br/><span style="position:relative;font-size:16px;margin-right:10px;top:-5px;">WEEKLY STATEMENT</span></div></td>
</tr>
</table>
<table cellpadding="1" cellspacing="0" style="width:100%;">
<tr>
<td style="width:100%;height:10%;"></td>
</tr>
</table>';

?>

<!-- CHARTS START =========================================================================== -->


<script type="text/javascript">

    var sm_num = "<?php echo $statement_number;?>";

    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {

      /*var data = google.visualization.arrayToDataTable([
        ['Element', 'Density', { role: 'style' }],
        ['Copper', 8.94, '#b87333', ],
        ['Silver', 10.49, 'silver'],
        ['Gold', 19.30, 'gold'],
        ['Platinum', 21.45, 'color: #e5e4e2' ]
      ]);
      */

      <?php

        $chart_string_white = '<img src="images/graph_0_percent_white.png">';
        $chart_string_grey = '<img src="images/graph_0_percent_grey.png">';
        $chart_string1 = '<div id="chart_div" style="width:100%;"></div>';
        $chart_string2 = '<div id="chart_div2" style="width:100%;"></div>';
        $chart_string3 = '<div id="chart_div3" style="width:100%;"></div>';
        $chart_string4 = '<div id="chart_div4" style="width:100%;"></div>';
        $chart_string5 = '<div id="chart_div5" style="width:100%;"></div>';
        $chart_string6 = '<div id="chart_div6" style="width:100%;"></div>';
        $chart_string7 = '<div id="chart_div7" style="width:100%;"></div>';
        $chart_string8 = '<div id="chart_div8" style="width:100%;"></div>';
        

      ?>

      var data = new google.visualization.DataTable();
      data.addColumn('string', '');    
      data.addColumn('number', '');
      data.addRows([
        ['VS', 130000],
        ['MS', 45000]
      ]);

      // Visa Chargeback Ratio, by transaction value
      /*
      var data_ChargebackRatio_VISA_value = new google.visualization.DataTable();
        data_ChargebackRatio_VISA_value.addColumn('string', ''); 
        data_ChargebackRatio_VISA_value.addColumn('number', '');
        data_ChargebackRatio_VISA_value.addRows([
                ['SLS',    34000],
                ['CB',12000],
                ['RF',8000]
        ]);
        */

        // Visa Fraud Ratio, by transaction value
        var data_Fraud_VISA_value = new google.visualization.DataTable();
        data_Fraud_VISA_value.addColumn('string', ''); 
        data_Fraud_VISA_value.addColumn('number', '');
        data_Fraud_VISA_value.addRows([
                ['FRD',     <?php echo $fraud_current_month_VISA_array['sum'];?>],
                ['TRNS',<?php echo $fraud_trns_current_month_VISA_array['sum'];?>]
        ]);

        // Visa Fraud Ratio, by transaction count
        var data_Fraud_VISA_count = new google.visualization.DataTable();
        data_Fraud_VISA_count.addColumn('string', ''); 
        data_Fraud_VISA_count.addColumn('number', '');
        data_Fraud_VISA_count.addRows([
                ['FRD',     <?php echo $fraud_current_month_VISA_array['count'];?>],
                ['TRNS',<?php echo $fraud_trns_current_month_VISA_array['count'];?>]
        ]);

        // Mastercard Fraud Ratio, by transaction value
        var data_Fraud_MC_value = new google.visualization.DataTable();
        data_Fraud_MC_value.addColumn('string', ''); 
        data_Fraud_MC_value.addColumn('number', '');
        data_Fraud_MC_value.addRows([
                ['FRD',     <?php echo $fraud_current_month_MC_array['sum'];?>],
                ['TRNS',<?php echo $fraud_trns_last_month_MC_array['sum'];?>]
        ]);

        // Mastercard Fraud Ratio, by transaction count
        var data_Fraud_MC_count = new google.visualization.DataTable();
        data_Fraud_MC_count.addColumn('string', ''); 
        data_Fraud_MC_count.addColumn('number', '');
        data_Fraud_MC_count.addRows([
                ['FRD',     <?php echo $fraud_current_month_MC_array['count'];?>],
                ['TRNS',<?php echo $fraud_trns_last_month_MC_array['count'];?>]
        ]);

        // Visa Chargeback Ratio, by transaction value
        var data_Chargeback_VISA_value = new google.visualization.DataTable();
        data_Chargeback_VISA_value.addColumn('string', ''); 
        data_Chargeback_VISA_value.addColumn('number', '');
        data_Chargeback_VISA_value.addRows([
                ['CB',     <?php echo $chargebacks_current_month_VISA_array['sum'];?>],
                ['TRNS',<?php echo $chargebacks_trns_current_month_VISA_array['sum'];?>]
        ]);

        // Visa Chargeback Ratio, by transaction count
        /*
      var data_ChargebackRatio_VISA_count = new google.visualization.DataTable();
      data_ChargebackRatio_VISA_count.addColumn('string', ''); 
      data_ChargebackRatio_VISA_count.addColumn('number', '');
      data_ChargebackRatio_VISA_count.addRows([
                ['SLS',     30],
                ['CB',24],
                ['RF',17]
        ]);
        */

      // Visa Chargeback Ratio, by transaction count
      var data_Chargeback_VISA_count = new google.visualization.DataTable();
      data_Chargeback_VISA_count.addColumn('string', ''); 
      data_Chargeback_VISA_count.addColumn('number', '');
      data_Chargeback_VISA_count.addRows([
              ['CB',     <?php echo $chargebacks_current_month_VISA_array['count'];?>],
              ['TRNS',<?php echo $chargebacks_trns_current_month_VISA_array['count'];?>]
      ]);  

        // Mastercard Chargeback Ratio, by transaction value
        var data_Chargeback_MC_value = new google.visualization.DataTable();
        data_Chargeback_MC_value.addColumn('string', ''); 
        data_Chargeback_MC_value.addColumn('number', '');
        data_Chargeback_MC_value.addRows([
                ['CB',     <?php echo $chargebacks_current_month_MC_array['sum'];?>],
                ['TRNS',<?php echo $chargebacks_trns_last_month_MC_array['sum'];?>]
        ]);

        // Mastercard Chargeback Ratio, by transaction count
        var data_Chargeback_MC_count = new google.visualization.DataTable();
        data_Chargeback_MC_count.addColumn('string', ''); 
        data_Chargeback_MC_count.addColumn('number', '');
        data_Chargeback_MC_count.addRows([
                ['CB',     <?php echo $chargebacks_current_month_MC_array['count'];?>],
                ['TRNS',<?php echo $chargebacks_trns_last_month_MC_array['count'];?>]
        ]);

/*
      var options = {
        title: "Density of Precious Metals, in g/cm^3",
        bar: {groupWidth: '95%'},
        legend: 'none',
        width:'100%'
      }; */

      var options = {
          //legend:{position: 'bottom',alignment: "center"},
          chartArea:{left:0,top:5,width:"100%",height:"100%"},
          //IMPORTANT - CHANGE ONLY THE WIDTH & HEIGHT BELOW TO CHANGE SIZE OF GRAPH
          width:140,
          height:140,

          pieSliceText: 'value',
          legend: {
                position: 'none',
                //labeledValueText: 'both',
                textStyle: {
                        color: 'blue',
                        fontSize: 11
                }
          }
          //, color : {'blue','orange','yellow'}
          ,slices: [{color: '#dc3912'}, {color: '#3366cc'}, {color: '#ff9900'}]
          
        };

        var options_CB = {
          //legend:{position: 'bottom',alignment: "center"},
          chartArea:{left:0,top:5,width:"100%",height:"100%"},
          //IMPORTANT - CHANGE ONLY THE WIDTH & HEIGHT BELOW TO CHANGE SIZE OF GRAPH
          width:140,
          height:140,

          pieSliceText: 'value',
          legend: {
                position: 'none',
                //labeledValueText: 'both',
                textStyle: {
                        color: 'blue',
                        fontSize: 11
                }
          }
          //, color : {'blue','orange','yellow'}
          ,slices: [{color: '#dc3912'}, {color: '#3366cc'}, {color: '#ff9900'}]
          ,backgroundColor: {fill: '#ececec',fillOpacity: 0.7}
          
        };

      var chart_div = document.getElementById('chart_div');
      //var chart = new google.visualization.ColumnChart(chart_div);
      
      var chart = new google.visualization.PieChart(chart_div);

      /*
      var path_base64 = "";  

      function saveToPHP( imgdata ) {

        var script = document.createElement("SCRIPT");

        alert(imgdata);

        script.setAttribute( 'type', 'text/javascript' );
        script.setAttribute( 'src', 'save.php?data=' + imgdata );

        document.head.appendChild( script );
      }

      function save() {

        var canvas = document.getElementById("chart_div"), // Get your canvas
                imgdata = canvas.toDataURL();

        saveToPHP( imgdata );
      }
      */

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart, 'ready', function () {
        path_base64 = chart.getImageURI();

        //chart_div.innerHTML = '<img src="' + path_base64 + '">';
        //chart_div.innerHTML = '<img src="images/graph_0_percent_grey.png">';

        <?php 

                //$js = 'chart_div.innerHTML = \'<img src="images/graph_0_percent_grey.png">\';';
                //$js = "chart_div.innerHTML = '<div style='font-family:open sans;font-size:12px;color:white;'>0%</div>";

                $js = "";
                //$fraud_current_month_VISA_array['sum']
                //$fraud_trns_current_month_VISA_array['sum']
                if ($fraud_current_month_VISA_array['sum'] == 0 && $fraud_trns_current_month_VISA_array['sum'] == 0) {
                        $js = 'chart_div.innerHTML = \'<img src="images/graph_0_percent_grey.png">\';';
                } else {
                        $js = 'chart_div.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>

        console.log(chart_div.innerHTML);
      });

      chart.draw(data_Fraud_VISA_value, options_CB);

      var chart_div2 = document.getElementById('chart_div2');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart2 = new google.visualization.PieChart(chart_div2);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart2, 'ready', function () {
        //chart_div2.innerHTML = '<img src="' + chart2.getImageURI() + '">';

        var path_base64 = chart2.getImageURI();

        <?php 
                $js = "";
                if ($fraud_current_month_VISA_array['count'] == 0 && $fraud_trns_current_month_VISA_array['count'] == 0) {
                        $js = 'chart_div2.innerHTML = \'<img src="images/graph_0_percent_grey.png">\';';
                } else {
                        $js = 'chart_div2.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div2.innerHTML);
      });

      chart2.draw(data_Fraud_VISA_count, options_CB);

      var chart_div3 = document.getElementById('chart_div3');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart3 = new google.visualization.PieChart(chart_div3);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart3, 'ready', function () {
        //chart_div3.innerHTML = '<img src="' + chart3.getImageURI() + '">';

        var path_base64 = chart3.getImageURI();

        <?php 
                $js = "";
                if ($fraud_current_month_MC_array['sum'] == 0 && $fraud_trns_last_month_MC_array['sum'] == 0) {
                        $js = 'chart_div3.innerHTML = \'<img src="images/graph_0_percent_white.png">\';';
                } else {
                        $js = 'chart_div3.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div3.innerHTML);
      });

      chart3.draw(data_Fraud_MC_value, options);

      var chart_div4 = document.getElementById('chart_div4');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart4 = new google.visualization.PieChart(chart_div4);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart4, 'ready', function () {
        chart_div4.innerHTML = '<img src="' + chart4.getImageURI() + '">';

        var path_base64 = chart4.getImageURI();

        <?php 
                $js = "";
                if ($fraud_current_month_MC_array['count'] == 0 && $fraud_trns_last_month_MC_array['count'] == 0) {
                        $js = 'chart_div4.innerHTML = \'<img src="images/graph_0_percent_white.png">\';';
                } else {
                        $js = 'chart_div4.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div4.innerHTML);
      });

      chart4.draw(data_Fraud_MC_count, options);

      /*
      var chart_div5 = document.getElementById('chart_div5');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart5 = new google.visualization.PieChart(chart_div5);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart5, 'ready', function () {
        chart_div5.innerHTML = '<img src="' + chart5.getImageURI() + '">';
        console.log(chart_div5.innerHTML);
      });

      chart5.draw(data, options);
      */

      var chart_div5 = document.getElementById('chart_div5');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart5 = new google.visualization.PieChart(chart_div5);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart5, 'ready', function () {
        //chart_div5.innerHTML = '<img src="' + chart5.getImageURI() + '">';

        var path_base64 = chart5.getImageURI();

        <?php 
                $js = "";
                if ($chargebacks_current_month_VISA_array['sum'] == 0 && $chargebacks_trns_current_month_VISA_array['sum'] == 0) {
                        $js = 'chart_div5.innerHTML = \'<img src="images/graph_0_percent_white.png">\';';
                } else {
                        $js = 'chart_div5.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div5.innerHTML);
      });

      chart5.draw(data_Chargeback_VISA_value, options);

      /*

      var chart_div6 = document.getElementById('chart_div6');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart6 = new google.visualization.PieChart(chart_div6);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart6, 'ready', function () {
        chart_div6.innerHTML = '<img src="' + chart6.getImageURI() + '">';
        console.log(chart_div6.innerHTML);
      });

      chart6.draw(data, options);
      */

      var chart_div6 = document.getElementById('chart_div6');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart6 = new google.visualization.PieChart(chart_div6);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart6, 'ready', function () {
        //chart_div6.innerHTML = '<img src="' + chart6.getImageURI() + '">';

        var path_base64 = chart6.getImageURI();

        <?php 
                $js = "";
                if ($chargebacks_current_month_VISA_array['count'] == 0 && $chargebacks_trns_current_month_VISA_array['count'] == 0) {
                        $js = 'chart_div6.innerHTML = \'<img src="images/graph_0_percent_white.png">\';';
                } else {
                        $js = 'chart_div6.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div6.innerHTML);
      });

      chart6.draw(data_Chargeback_VISA_count, options);

      /*
      var chart_div7 = document.getElementById('chart_div7');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart7 = new google.visualization.PieChart(chart_div7);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart7, 'ready', function () {
        chart_div7.innerHTML = '<img src="' + chart7.getImageURI() + '">';
        console.log(chart_div7.innerHTML);
      });

      chart7.draw(data, options);
      */

      var chart_div7 = document.getElementById('chart_div7');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart7 = new google.visualization.PieChart(chart_div7);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart7, 'ready', function () {
        //chart_div7.innerHTML = '<img src="' + chart7.getImageURI() + '">';

        var path_base64 = chart7.getImageURI();

        <?php 
                $js = "";
                if ($chargebacks_current_month_MC_array['sum'] == 0 && $chargebacks_trns_last_month_MC_array['sum'] == 0) {
                        $js = 'chart_div7.innerHTML = \'<img src="images/graph_0_percent_grey.png">\';';
                } else {
                        $js = 'chart_div7.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div7.innerHTML);
      });

      chart7.draw(data_Chargeback_MC_value, options_CB);

      /*
      var chart_div8 = document.getElementById('chart_div8');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart8 = new google.visualization.PieChart(chart_div8);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart8, 'ready', function () {
        chart_div8.innerHTML = '<img src="' + chart8.getImageURI() + '">';
        console.log(chart_div8.innerHTML);
      });

      chart8.draw(data, options);
      */

      var chart_div8 = document.getElementById('chart_div8');
      //var chart = new google.visualization.ColumnChart(chart_div);
      var chart8 = new google.visualization.PieChart(chart_div8);

      // Wait for the chart to finish drawing before calling the getImageURI() method.
      google.visualization.events.addListener(chart8, 'ready', function () {
        //chart_div8.innerHTML = '<img src="' + chart8.getImageURI() + '">';

        var path_base64 = chart8.getImageURI();

        <?php 
                $js = "";
                if ($chargebacks_current_month_MC_array['count'] == 0 && $chargebacks_trns_last_month_MC_array['count'] == 0) {
                        $js = 'chart_div8.innerHTML = \'<img src="images/graph_0_percent_grey.png">\';';
                } else {
                        $js = 'chart_div8.innerHTML = \'<img src="\' + path_base64 + \'">\';';
                }

                echo $js;
        ?>
        console.log(chart_div8.innerHTML);
      });

      chart8.draw(data_Chargeback_MC_count, options_CB);

  }

        function MouseOver(elem) {
                document.body.style.cursor = "pointer";
        }

        function MouseOut(elem) {
                document.body.style.cursor = "default";
        }

        function btn_print() {
                var tempTitle = document.title;
                document.title = "" + sm_num +".pdf";
                window.print();
                document.title = tempTitle;
                
                //javascript:window.print()
        }

  </script>

<!-- CHARTS END ============================================================================= -->

</head>

<body bgcolor="#525659" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" >

<!-- Start of Main Table - Full Page-->
<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#525659">
<tr>
<td bgcolor="#525659" width="100%">

<div id="btnPrint" class="no-print" style="position:fixed;width:100%;top:10px;height:50px;padding:0px;z-index:10;">
        <div class="no-print" style="width:1%;margin-left:97%;z-index:50px;"><img src="images/printer.png" onClick="btn_print()" onmouseover="MouseOver(this);" onmouseout="MouseOut(this);"></div>
</div>

<!-- Start of Sub Table - Page width -->
<table style="width:755px;margin:0 auto;text-align:center;" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" align="center">
<tr>
<td bgcolor="#ffffff" width="100%">

<!-- WEEKLY STATEMENT HEADING -->
<table class="page_color" style="width:700px;"align="center"> <!-- border: 1px solid blue; -->
<tr>
<th class="transact24_heading_logo"><img src="images/t24logo.png" style="width:80%;"></th>
<th class="heading_monthly_stm">WEEKLY STATEMENT</th>
</tr>
</table>

<?php

$html = '<!-- TABLE TO CENTER STATEMENT -->
<table style="width:700px;text-align:center;margin:0 auto;" align="center">
<tr>
<td>

<table class="first" cellpadding="7" style="width:700px;text-align:left;">
<tr>
        <td style="width:50%;">
                <table cellspacing="2">
                <tr><td class="textwhite"><strong>'.$assoc_array['beneficiary_name'].'</strong></td></tr>
                <tr><td class="textwhite indentabit">'.$assoc_array['company_address'].'</td></tr>
                <tr><td class="textwhite indentabit">'.$assoc_array['postal_code'].'</td></tr>
                <tr><td class="textwhite indentabit">Contact Person: '.$assoc_array['contact_person_name'].'</td></tr>
                <tr><td class="textwhite indentabit italictext">'.$assoc_array['contact_person_email'].'</td></tr>
                </table>
        </td>
        <td style="width:50%;">
                <table cellpadding="2" style="width:100%;border-spacing: 0px 5px;">
                <tr class="rowbg" ><td class="textwhiteleft">Statement Date</td><td class="textwhiteright">'.$datenow.'</td></tr>
                </table>
                <table cellspacing="0" cellpadding="3" style="width:100%;">
                <tr class="rowbg"><td class="textwhiteleft borderbot">Processing Period</td><td class="textwhiteright borderbot" >'.$assoc_array['fromdate'].' to '.$assoc_array['todate'].'</td></tr>
                <tr class="rowbg"><td class="textwhiteleft borderbot">Statement Number</td><td class="textwhiteright borderbot">'.$yearnow.'/'.$assoc_array['statement_no'].'/'.$assoc_array['merchant_mapping_ref_code'].'</td></tr>
                <tr class="rowbg"><td class="textwhiteleft borderbot">MID ID</td><td class="textwhiteright borderbot"> '.$assoc_array['live_mid']." (".$assoc_array['merchant_name'].')</td></tr>
                <tr class="rowbg"><td class="textwhiteleft">URL</td><td class="textwhiteright"> '.$assoc_array['website_address'].'</td></tr>
                </table>
        </td>
</tr>
</table>
<table style="color:#40a793;width:700px;text-align:left;">
<tr>
        <td style="line-height:2px;"></td>
</tr>
</table>

<table style="color:#40a793;border-spacing:0 7px;width:700px;text-align:left;">
<tr>
        <td style="font-size:15px;font-weight:bold;width:70%;border-bottom: 2px solid #82a29c;">BALANCE BROUGHT FORWARD</td>
        <td style="font-size:12px;border-bottom: 2px solid #82a29c;width:15%;text-align:right;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td style="font-size:12px;border-bottom: 2px solid #82a29c;width:15%;text-align:right;'.$neg_bal_brought_fwd_str.number_format($assoc_array['statement_bbf_amount'],2).'</td>
</tr>
</table>
<table style="color:#40a793;border-spacing:0 0px;width:700px;text-align:left;">
<tr>
        <td style="font-size:15px;font-weight:bold;width:70%;border-bottom: 2px solid #82a29c;">TOTAL DUE FOR REMITTANCE</td>
        <td style="border-bottom: 2px solid #82a29c;width:15%;text-align:right;font-weight: bold;font-size:16px;color:#1e6459;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td style="border-bottom: 2px solid #82a29c;width:15%;text-align:right;font-weight: bold;font-size:16px;color:#1e6459;'.$neg_tot_due_rem_str.number_format($assoc_array['total_due_remittance'],2).'</td>
</tr>
</table>
<table style="color:#40a793;border-spacing:0 7px;width:700px;text-align:left;">
<tr>
        <td style="font-size:15px;font-weight:bold;border-bottom: 2px solid #82a29c;">TOTALS</td>
        <td style="font-size:14px;border-bottom: 2px solid #82a29c;"></td>
        <td style="font-size:14px;border-bottom: 2px solid #82a29c;"></td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="width:10%;text-align:center;">Items</td>
        <td class="columnheadings" style="width:13%;text-align:center;">Amount</td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="width:13%;text-align:center;">Amount</td>
        <td class="columnheadings"  style="width:13%;"></td>
</tr>
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="position:relative;top:-8px;width:10%;text-align:center;"></td>
        <td class="columnheadings" style="position:relative;top:-4px;width:13%;text-align:center;"><span style="font-size:8px;font-weight:bold;">Authorisation Currency</span></td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="position:relative;top:-4px;width:13%;text-align:center;"><span style="font-size:8px;font-weight:bold;">Settlement Currency</span></td>
        <td class="columnheadings"  style="width:13%;"></td>
</tr>
</table>
<!--tr class="bgrowoddd">
       <td class="titleitems" style="width:35%;color:#40a793;">Transactions</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
</tr-->
<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Sales</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$sales_array['recon_count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($sales_array['recon_sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($sales_array['recon_sum'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Declines</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$declines_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($declines_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($declines_array['recon_sum'],2).'</td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Refunds</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$refunds_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_refund_trn_totals_str.number_format($refunds_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_refund_recon_totals_str.number_format($refunds_array['recon_sum'],2).'</td>
</tr>
<!--tr class="bgrowoddd">
       <td class="titleitems" style="width:35%;color:#40a793;">Disputes</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
</tr-->
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Fraud Alerts</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$fraudalers_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_array['recon_sum'],2).'</td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Chargebacks</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargebacks_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_chargeback_sum_str.number_format($chargebacks_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_chargeback_recon_str.number_format($chargebacks_array['sum'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Chargeback Reversals</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargeback_reversals_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($chargeback_reversals_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($chargeback_reversals_array['recon_sum'],2).'</td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Representments</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$representments_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($representments_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($representments_array['recon_sum'],2).'</td>
</tr>
<tr>
       <td class="titleitems" style="width:35%;text-indent: 10px;">Adjustments</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$adj_items.'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_adj_auth_amt_str.number_format($adj_auth_amt,2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;'.$neg_adj_settle_amt_str.number_format($adj_settle_amt,2).'</td>
</tr>
<tr>
       <td rowspan="2" colspan="7" class="titleitems" style="width:100%;border-bottom: 1px solid #DCDCDC;"><table><tr><td style="width:2%;">&nbsp;</td><td style="width:98%;">'.$adj_note.'</td></tr></table></td>
</tr>
<tr>
        <td style="line-height: 2px;"></td>
</tr>
</table>
<br/>
<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings">FEES in  '.substr($assoc_array['settlement_currency'], -3).'</th>
        <td class="columnheadings" style="width:10%;text-align:center;">Items</th>
        <td class="columnheadings"  style="width:10%;">Fee</th>
        <td class="columnheadings"  style="width:45%;text-align:right;">Total</th>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;">Merchant Discount Rate</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;">'.$assoc_array['mdr_visa'].'%</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_merchant_disc_rate_str.number_format($assoc_array['mdr_total'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;color:#40a793;">Transaction Fees</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;"></td>
        <td class="titleitems" style="width:45%;text-align:right;"></td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Approved Transactions</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$sales_array['count'].'</td>
        <td class="titleitems" style="width:10%;">'.$assoc_array['transaction_fee_n3d_secure_approved'].'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_approved_trns_fees_str.number_format($assoc_array['approved_trn_total'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Declined Transactions</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$declines_array['count'].'</td>
        <td class="titleitems" style="width:10%;">'.$assoc_array['transaction_fee_n3d_secure_declined'].'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_declined_trns_fees_str.number_format($assoc_array['declined_trn_total'],2).'</td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Refund Transactions</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$refunds_array['count'].'</td>
        <td class="titleitems" style="width:10%;">'.$assoc_array['refund_fee'].'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_refund_trns_fees_str.number_format($assoc_array['refund_trn_total'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;color:#40a793;">Dispute Fees</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;"></td>
        <td class="titleitems" style="width:45%;text-align:right;"></td>               
</tr>';

$bgrowodd = 1;
$tr = ' <tr class="bgrowoddd">';

$html .= $tr.'<td class="titleitems" style="width:35%;text-indent: 10px;">Fraud Alerts</td>
<td class="titleitems" style="width:10%;text-align:center;">'.$fraudalers_array['count'].'</td>
<td class="titleitems" style="width:10%;">'.number_format($assoc_array['fraud_alert_fee'],2).'</td>
<td class="titleitems" style="width:45%;text-align:right;'.$neg_fraud_alert_disp_fees_str.number_format($assoc_array['fraudalert_trn_total'],2).'</td>                
</tr>';

$cb_on = 0;

if ($chargebacks_per_month_array['count'] < 100)
{

        if ($bgrowodd == 0) {
                $tr = ' <tr class="bgrowoddd">';
                $bgrowodd = 1;
        } else {
                $tr = ' <tr >';
                $bgrowodd = 0;
        }

        $html .= $tr.'<td class="titleitems" style="width:35%;text-indent: 10px;">Chargebacks < 100 per month</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargebacks_array['count'].'</td>
        <td class="titleitems" style="width:10%;">'.number_format($assoc_array['chargeback_fee'],2).'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_chargebacks_disp_fees_str.number_format($assoc_array['chargebacks_trn_total'],2).'</td>                
                </tr>'; //removed class="bgrowoddd"
}
if ($chargebacks_per_month_array['count'] >= 100)
{
        $cb_on = 1;
        $cb_tot = $chargebacks_per_month_array['count'] - 100;

        if ($bgrowodd == 0) {
                $tr = ' <tr class="bgrowoddd">';
                $bgrowodd = 1;
        } else {
                $tr = ' <tr >';
                $bgrowodd = 0;
        }

         $html .= $tr.'<td class="titleitems" style="width:35%;text-indent: 10px;">Chargebacks >= 100 per month</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$cb_tot.'</td>
        <td class="titleitems" style="width:10%;">'.number_format($assoc_array['chargeback_fee_more_1_percent'],2).'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_chargebacks_disp_fees_str2.number_format($assoc_array['chargebacks2_trn_total'],2).'</td>               
                </tr>';
}

if ($bgrowodd == 0) {
        $tr = ' <tr class="bgrowoddd">';
        $bgrowodd = 1;
} else {
        $tr = ' <tr >';
        $bgrowodd = 0;
}

$html .= $tr.'<td class="titleitems" style="width:35%;text-indent: 10px;">Chargeback Representments</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$representments_array['count'].'</td>
        <td class="titleitems" style="width:10%;">'.number_format($assoc_array['chargeback_representment_fee'],2).'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_chargeback_rep_str.number_format($assoc_array['representment_total'],2).'</td>                
        </tr>';

if ($bgrowodd == 0) {
        $tr = ' <tr class="bgrowoddd">';
        $bgrowodd = 1;
} else {
        $tr = ' <tr >';
        $bgrowodd = 0;
}

$html .= $tr.'<td class="titleitems" style="width:35%;text-indent: 10px;">Chargeback Reversals</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargeback_reversals_array['count'].'</td>
        <td class="titleitems" style="width:10%;">0.00</td>
        <td class="titleitems" style="width:45%;text-align:right;">0.00</td>                
        </tr>';

if ($bgrowodd == 0) {
        $tr = ' <tr class="bgrowoddd">';
        $bgrowodd = 1;
} else {
        $tr = ' <tr >';
        $bgrowodd = 0;
}

 $html .= $tr.'<td class="titleitems" style="width:35%;">Wire Transfer Fee</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;">'.number_format($assoc_array['wire_transfer_fee'],2).'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_wire_transfer_fee_str.number_format($assoc_array['wire_transfer_fee'],2).'</td>               
</tr>';

if ($bgrowodd == 0) {
        $tr = ' <tr class="bgrowoddd">';
        $bgrowodd = 1;
} else {
        $tr = ' <tr >';
        $bgrowodd = 0;
}

 $html .= $tr.'<td class="titleitems" style="width:35%;">Monthly Statement Fee</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;">'.number_format($assoc_array['monthly_statement_fee'],2).'</td>
        <td class="titleitems" style="width:45%;text-align:right;'.$neg_monthly_statement_fee_str.number_format($assoc_array['monthly_statement_fee'],2).'</td>               
</tr>';

$html .= '<tr><td class="message_heading" style="width:35%;">Fees Total</td>
        <td class="titleitems" style="width:10%;text-align:center;"></td>
        <td class="titleitems" style="width:10%;"></td>
        <td class="titleitems" style="width:45%;text-align:right;font-size:12px !important;'.$neg_fees_total_str.number_format($fees_total,2).'</td>              
</tr>
</table>
<table style="color:#40a793;border-spacing:0 0px;width:700px;text-align:left;">
<tr>
        <td style="line-height:2px;border-bottom: 2px solid #82a29c;width:100%;">&nbsp;</td>
</tr>
</table>
<br/>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings feesborderbot" style="width:100%;">ROLLING RESERVE</td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings_sub" style="position:relative;top:4px;width:70%;">10%, 180 days</td>
        <td class="columnheadings"  style="position:relative;top:4px;width:15%;text-align:center;">Amount</td>
        <td class="columnheadings"  style="width:15%;"></td>
</tr>
<tr>
        <td class="columnheadings" style="width:70%;"></td>
        <td class="columnheadings"  style="position:relative;top:-2px;width:15%;text-align:center;"><span style="font-size:8px;">Settlement Currency</span></td>
        <td class="columnheadings"  style="width:15%;"></td>
</tr>
</table>
<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr class="bgrowoddd">
        <td class="titleitems" style="width:70%;text-indent: 10px;padding:2px;">Rolling Reserve Retained</td>
        <td class="currency_clr" style="width:15%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:15%;text-align:right;'.$neg_rolling_reserve_held_str.number_format($assoc_array['rolling_reserve_total'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:70%;text-indent: 10px;padding:2px;">Rolling Reserve Released</td>
        <td class="currency_clr" style="width:15%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:15%;text-align:right;">'.number_format($assoc_array['rolling_reserve_to_release'],2).'</td>
</tr>
</table>

<!-- BLANK LINE -->
<div class="blank_line"></div>';

echo $html;

//If there are no graphics to show, then only display the messages
//GRAPHICS AVAILABLE

$no_image_border1 = "";
$no_image_border2 = "";
$no_image_border3 = "";
$img_holder = "";
$blank_height = "20";

if ($graphics_yes == "yes" )
{

        if ($message_heading != "") {
                $no_image_border1 = "border: 2px solid #24685c;";
                $no_image_border2 = "border-right:1px solid #82a29c;";
                $no_image_border3 = "border-bottom: 1px solid #82a29c;";
                $img_holder = "../../upload/refunds1.png";
                $blank_height = "20";
        } else {
                $no_image_border1 = "border: 0px solid #24685c;";
                $no_image_border2 = "border-right:0px solid #82a29c;";
                $no_image_border3 = "border-bottom: 0px solid #82a29c;";
                $img_holder = "../../upload/img_blank.png";
                $blank_height = "60";
        }

        $html = '<table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;background-color: #dcdcdb;"><tr><td>

        <table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;'.$no_image_border1.'">
        <tr>
        <td style="width:70%;'.$no_image_border2.'">

        <table cellpadding="2" cellspacing="0" style="width:100%;">
        <tr>
                <td style="'.$no_image_border3.'text-align:left;width:100%;height:0px;text-indent:5px;margin-top:0px;margin-bottom:0px;">
                <span class="message_heading">'.$message_heading.'</span>
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:20px;font-size:11px;text-align:left;font-weight:bold;text-indent:20px;margin-bottom:0px;" class="titleitems">
                        '.$message_primary.'
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:20px;font-size:10px;text-align:left;text-indent:40px;margin-bottom:0px;" class="titleitems">
                        '.$message_primary_note.'
                </td>
        </tr>
        <tr>
                <td style="'.$no_image_border3.'text-align:left;">
                <span style="position:relative;left:2px;font-size:14px; color: #82a29c;"></span>
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:20px;font-size:10px;text-align:left;margin-bottom:0px;padding:5px;" class="titleitems">
                        <div style="position:relative;left:20px;width:90%;">'.$message_secondary.'</div>
                </td>
        </tr>
        </table>
        </td>
        <td style="width:30%;text-align:center;margin:0 auto;">
        <!-- INSERT GRAPHICS HERE -->
        <div style="position:relative;top:3px;width:200px;text-align:center;margin:0 auto;">
        <img src="'.$img_holder.'" style="width:100%;">
        </div>
        </td>
        </tr>
        </table>

        </td></tr>
        </table>

        <!-- BLANK SPACE -->
        <table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;height:'.$blank_height.'px;">
        <tr>
        <td>
        </td>
        </tr>
        </table>

        </td>
        </tr>
        </table> <!-- TABLE TO CENTER STATEMENT -->
        '.$page_counter_footer;

//---------------------------------------------------------------

} else {

        //NO GRAPHICS AVAILABLE
        //$td_style = '<td style="width:100%;">';
        //$grahics = '';

        if ($message_heading != "") {
                $no_image_border1 = "border: 2px solid #24685c;";
                $no_image_border2 = "border-bottom: 1px solid #82a29c;";
        } else {
                $no_image_border1 = "border: 0px solid #24685c;";
                $no_image_border2 = "border-bottom: 0px solid #82a29c;";
        }

        $html = '<table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;background-color: #dcdcdb;"><tr><td>

        <table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;'.$no_image_border1.'">
        <tr>
        <td style="width:100%;">

        <table cellpadding="2" cellspacing="0" style="width:100%;">
        <tr>
                <td style="'.$no_image_border2.'text-align:left;width:100%;height:0px;text-indent:5px;margin-top:0px;margin-bottom:0px;">
                <span style="position:relative;left:2px;font-size:14px; color: #82a29c;">'.$message_heading.'</span>
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:20px;font-size:11px;text-align:left;font-weight:bold;text-indent:20px;margin-bottom:0px;" class="titleitems">
                        '.$message_primary.'
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:20px;font-size:10px;text-align:left;text-indent:40px;margin-bottom:0px;" class="titleitems">
                        '.$message_primary_note.'
                </td>
        </tr>
        <tr>
                <td style="'.$no_image_border2.'text-align:left;">
                <span style="position:relative;left:2px;font-size:14px; color: #82a29c;"></span>
                </td>
        </tr>
        <tr>
                <td style="width:100%;height:30px;font-size:10px;text-align:left;" class="titleitems">
                <div style="position:relative;left:20px;width:90%;">'.$message_secondary.'</div>
                </td>
        </tr>
        </table>
        </td>
        </tr>
        </table>

        </td></tr>
        </table>

        </td>
        </tr>
        </table> <!-- TABLE TO CENTER STATEMENT -->
        '.$page_counter_footer;
}

echo $html;

?>

<!-- BLANK LINE -->
<div class="blank_line"></div>

<!-- PAGE BREAK TABLE -->
<!--table class="page_break no-print" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#525659" style="left:-100px;position:absolute;float:left;z-index:1000;">
<tr>
<td bgcolor="#525659" width="100%" style="height:20px;">
</td>
<td bgcolor="#ffffff" width="100%" style="height:20px;">
</td>
</tr>
</table-->

<table class="page_break no-print" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#525659">
<tr>
<td bgcolor="#525659" width="100%" style="height:20px;">
</td>
</tr>
</table>

<!-- BLANK LINE -->
<div class="blank_line"></div>

<?php

$html = $page_header.'
<div class="blank_line"></div>

<!-- TABLE TO CENTER STATEMENT -->
<table style="width:700px;text-align:center;margin:0 auto;" align="center">
<tr>
<td>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings feesborderbot" style="width:100%;">FRAUD</td>
</tr>
</table>

<div style="width:100%;height:4px;"></div>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="width:10%;text-align:center;">Items</td>
        <td class="columnheadings" style="width:13%;">Amount</td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="width:13%;">Amount</td>
        <td class="columnheadings"  style="width:13%;padding-bottom:0px;"></td>
</tr>
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="width:10%;text-align:center;"></td>
        <td class="columnheadings" style="width:13%;"><span style="position:relative;top:-3px;font-size:8px;">Authorisation Currency</span></td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="width:13%;"><span style="position:relative;top:-3px;font-size:8px;">Settlement Currency</span></td>
        <td class="columnheadings"  style="width:13%;"></td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Fraud Alerts (Visa)</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$fraudalers_VISA_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_VISA_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_VISA_array['recon_sum'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Fraud Alerts (Mastercard)</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$fraudalers_MC_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['auth_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_MC_array['sum'],2).'</td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($fraudalers_MC_array['recon_sum'],2).'</td>
</tr>
</table>

<div class="blank_line_trans"></div>

<!-- INDENT TABLE
<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
<td style="text-indent:40px;"-->

<table cellpadding="0" cellspacing="0" style="width:100%;text-align:left;">
<tr>
        <th class="headertable" style="width:100%;"><span style="font-size:11px;padding-left:4px;">Monthly Calculation</span></th>
</tr>
<tr>
        <th class="feesborderbot" style="width:100%;"></th>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:center;">
<tr>
        <td style="width:50%;background-color:#ececec">
                <table cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                        <td colspan="2" style="width:50%;"><img src="./images/visa_logo_2018.png"></td>
                </tr>
                </table>
        </td>
        <td style="width:50%;border-left:1px solid #82a29c;">
                <table style="width:100%;">
                <tr>
                        <td colspan="2" style="width:50%;"><img src="./images/mastercard_logo_2018.png"></td>
                </tr>
                </table>
        </td>
</tr>
</table>';

echo $html;

?>

<table cellpadding="0" cellspacing="0" style="position:relative;top:0px;width:700px;text-align:center;" align="center">
<tr>
        <td style="width:50%;text-align:center;padding:0px;margin:0;background-color:#ececec;">
                <table style="width:100%;">
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Fraud Ratio <?php if (is_nan($fraud_ratio_perc_VISA_value)) { echo "0%"; } else { echo $fraud_ratio_perc_VISA_value.'%'; } ?></td>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Fraud Ratio <?php if (is_nan($fraud_ratio_perc_VISA_count)) { echo "0%"; } else { echo $fraud_ratio_perc_VISA_count.'%'; } ?></td>
                </tr>
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction value<i/></td>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction count<i/></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;"><div id="chart_div" style="width:100%;"></div></td>
                        <td colspan="2" style="width:50%;text-align:center;"><div id="chart_div2" style="width:100%;"></div></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_of_month_str;?> - <?php echo $end_date_of_month_str;?></td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($fraud_current_month_VISA_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Fraud Alerts</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($fraud_trns_current_month_VISA_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Total Sales</td></tr>
                                </table>
                        </td>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_of_month_str;?> - <?php echo $end_date_of_month_str;?></td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $fraud_current_month_VISA_array['count'];?><td class="textLeft" style="width:50%;font-size:9px;">&nbsp;| Frauds count</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $fraud_trns_current_month_VISA_array['count'];?></td><td class="textLeft" style="width:50%;font-size:9px;">&nbsp;| Total transactions</td></tr>
                                </table>
                        </td>
                </tr>
                </table>
        </td>
        <td style="width:50%;border-left:1px solid #82a29c;text-align:center;">
                <table style="width:100%;">
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Fraud Ratio <?php if (is_nan($fraud_ratio_perc_MC_value)) { echo "0%"; } else { echo $fraud_ratio_perc_MC_value.'%'; } ?></td>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Fraud Ratio <?php if (is_nan($fraud_ratio_perc_MC_count)) { echo "0%"; } else { echo $fraud_ratio_perc_MC_count.'%'; } ?></td>
                </tr>
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction value</i></td>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction count</i></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div3' style="width:100%;"></div></td>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div4' style="width:100%;"></div></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_last_month_str;?> - <?php echo $end_date_of_last_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($fraud_current_month_MC_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Fraud Alerts</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($fraud_trns_last_month_MC_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"> <?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Total Sales</td></tr>
                                </table>
                        </td>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_last_month_str;?> - <?php echo $end_date_of_last_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $fraud_current_month_MC_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Frauds count</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $fraud_trns_last_month_MC_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Total transactions</td></tr>
                                </tr>
                                </table>
                        </td>
                </tr>
                </table>
        </td>
</tr>
</table>

<div class="blank_line"></div>

<?php

$html = '

<!-- END OF INDENT TABLE 
</td>
</tr>
</table-->

<!-- BACKGROUND GREY TABLE FOR CHARGEBACKS
<table cellpadding="0" cellspacing="0" style="position:relative;width:740px;z-index:0;">
<tr>
<td-->

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;padding-top:2px;">
<tr>
        <td class="columnheadings" style="width:100%;padding-left:4px;">CHARGEBACKS</td>
</tr>
<tr>
        <td class="feesborderbot" style="width:100%;"></td>
</tr>
</table>

<div style="width:100%;height:4px;"></div>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="width:10%;text-align:center;">Items</td>
        <td class="columnheadings" style="width:13%;"></td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="width:13%;">Amount</td>
        <td class="columnheadings"  style="width:13%;"></td>
</tr>
<tr>
        <td class="columnheadings" style="width:35%;"></td>
        <td class="columnheadings" style="width:10%;text-align:center;"></td>
        <td class="columnheadings" style="width:13%;"></td>
        <td class="columnheadings"  style="width:13%;"></td>
        <td class="columnheadings"  style="width:3%;"></td>
        <td class="columnheadings"  style="width:13%;"><span style="position:relative;top:-2px;font-size:8px;">Chargeback Currency</span></td>
        <td class="columnheadings"  style="width:13%;"></td>
</tr>
<tr class="bgrowoddd">
        <td class="titleitems" style="width:35%;text-indent: 10px;">Chargeback (Visa)</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargebacks_VISA_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($chargebacks_VISA_array['recon_sum'],2).'</td>
</tr>
<tr>
        <td class="titleitems" style="width:35%;text-indent: 10px;">Chargeback (Mastercard)</td>
        <td class="titleitems" style="width:10%;text-align:center;">'.$chargebacks_MC_array['count'].'</td>
        <td class="currency_clr" style="width:13%;text-align:center;"></td>
        <td class="titleitems" style="width:13%;text-align:right;"></td>
        <td class="titleitems" style="width:3%;text-align:center;"></td>
        <td class="currency_clr" style="width:13%;text-align:center;">'.substr($assoc_array['settlement_currency'], -3).'</td>
        <td class="titleitems" style="width:13%;text-align:right;">'.number_format($chargebacks_MC_array['recon_sum'],2).'</td>
</tr>
</table>

<div class="blank_line_trans"></div>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
<tr>
        <th class="headertable" style="width:100%;"><span style="font-size:11px;padding-left:4px;">Monthly Calculation</span></th>
</tr>
<tr>
        <th class="feesborderbot" style="width:100%;"></td>
</tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:700px;text-align:center;">
<tr>
        <td style="width:50%;">
                <table style="width:100%;">
                <tr>
                        <td colspan="2" style="width:50%;"><img src="./images/visa_logo_2018.png"></td>
                </tr>
                </table>
        </td>
        <td style="width:50%;border-left:1px solid #82a29c;background-color:#ececec">
                <table style="width:100%;">
                <tr>
                        <td colspan="2" style="width:50%;"><img src="./images/mastercard_logo_cb_2018.png"></td>
                </tr>
                </table>
        </td>
</tr>
</table>';

echo $html;

?>

<table cellpadding="0" cellspacing="0" style="position:relative;top:-10px;width:700px;text-align:center;" align="center">
<tr>
        <td style="width:50%;text-align:center;;">
                <table style="width:100%;">
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Chargeback Ratio <?php if (is_nan($chargeback_ratio_perc_VISA_value)) { echo "0%"; } else { echo $chargeback_ratio_perc_VISA_value.'%'; } ?></td>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Chargeback Ratio <?php if (is_nan($chargeback_ratio_perc_VISA_count)) { echo "0%"; } else { echo $chargeback_ratio_perc_VISA_count.'%'; } ?></td>
                </tr>
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction value<i/></td>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction count<i/></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div5' style="width:100%;"></div></td>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div6' style="width:100%;"></div></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_of_month_str;?> - <?php echo $end_date_of_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($chargebacks_current_month_VISA_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Chargebacks</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($chargebacks_trns_current_month_VISA_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Total Sales</td></tr>
                                </table>
                        </td>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_of_month_str;?> - <?php echo $end_date_of_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $chargebacks_current_month_VISA_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Chargebacks count</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $chargebacks_trns_current_month_VISA_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Total transactions</td></tr>

                                </table>
                        </td>
                </tr>
                </table>
        </td>
        <td style="width:50%;border-left:1px solid #82a29c;text-align:center;background-color:#ececec">
                <table style="width:100%;">
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Chargeback Ratio <?php if (is_nan($chargeback_ratio_perc_MC_value)) { echo "0%"; } else { echo $chargeback_ratio_perc_MC_value.'%'; } ?></td>
                        <td style="width:10%;"></td>
                        <td class="chart_title_font" style="width:40%;text-align:left;">Chargeback Ratio <?php if (is_nan($chargeback_ratio_perc_MC_count)) { echo "0%"; } else { echo $chargeback_ratio_perc_MC_count.'%'; } ?></td>
                </tr>
                <tr>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction value</i></td>
                        <td style="width:10%;"></td>
                        <td class="chart_subtitle_font" style="width:40%;text-align:left;"><i>by transaction count</i></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div7' style="width:100%;"></div></td>
                        <td colspan="2" style="width:50%;text-align:center;"><div id='chart_div8' style="width:100%;"></div></td>
                </tr>
                <tr>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_last_month_str;?> - <?php echo $end_date_of_last_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($chargebacks_current_month_MC_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Chargebacks</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo number_format($chargebacks_trns_last_month_MC_array['sum'],2);?></td><td class="textLeft" style="width:50%;font-size:9px;"><?php echo " ".substr($assoc_array['settlement_currency'], -3)." |";?>&nbsp;Total Sales</td></tr>

                                </table>
                        </td>
                        <td colspan="2" style="width:50%;text-align:center;">
                                <table cellpadding="0" cellspacing="0" class="textCenter" align="center" style="width:100%;">
                                <tr class="spaceUnder titleitems"><td colspan="3" style="font-size:9px;">&nbsp; <?php echo $start_date_last_month_str;?> - <?php echo $end_date_of_last_month_str;?> &nbsp;</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_red"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $chargebacks_current_month_MC_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Chargebacks count</td></tr>
                                <tr class="spaceUnder titleitems"><td style="width:10%;"><div class="pie_options pie_c_frds_blue"></div></td><td class="textLeft" style="width:40%;font-size:9px;">&nbsp;<?php echo $chargebacks_trns_last_month_MC_array['count'];?></td><td class="textLeft"><font style="width:50%;font-size:9px;">&nbsp;| Total transactions</td></tr>

                                </table>
                        </td>
                </tr>
                </table>
        </td>
</tr>
</table>

<?php 

$page_counter = 2;

$page_counter_footer = set_page_counter_footer($page_counter);

$html = '

<!--/td>
</tr>
</table>  END BACKGROUND GRAY TABLE FOR CHARGEBACKS -->

</td>
</tr>
</table> <!-- TABLE TO CENTER STATEMENT -->'.$page_counter_footer;

echo $html;

?>

<!-- BLANK LINE -->
<div class="blank_line"></div>

<!-- PAGE BREAK TABLE -->
<table class="page_break no-print" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#525659">
<tr>
<td bgcolor="#525659" width="100%" style="height:20px;">
</td>
</tr>
</table>

<!-- BLANK LINE -->
<div class="blank_line"></div>

<?php

$html = $page_header.'
<!-- TABLE TO CENTER STATEMENT -->
<table style="width:700px;text-align:center;margin:0 auto;" align="center">
<tr>
<td>';

echo $html;

//Chargebacks Debited (Transaction ID's)
//Copy chargebacks debited array
$count_chargebacks_debited_copy = array();
if (count($chargebacks_debited_array) != 0) {

        $html = '<div class="blank_line"></div>
        <table cellpadding="0" cellspacing="0" style="width:700px;text-align:left;">
        <tr>
        <!--td class="note_text" style="width:100%;padding:0px;border-bottom: 1px solid #82a29c;"><span><i>Please note that the following Chargebacks, listed below by Transaction ID, have been debited from ('.$chargeback_row['merchant_name'].') under the `Adjustments` tab. </i></span></td-->
        <td class="note_text" style="width:100%;padding:0px;border-bottom: 1px solid #82a29c;"><span><i>Please note that the following Chargebacks, listed below by Transaction ID, have been debited from: </i></span></td>
        </tr>
        </table>
        <!-- BLANK LINE -->
        <div class="blank_line_trans"></div>';

        echo $html;

        $cols = 0;

        //$stm_fromdate = date('Y-m-d 00:00:00', strtotime($assoc_array['fromdate']));
        //$stm_todate = date('Y-m-d 23:59:59', strtotime($assoc_array['todate']));

        //echo $stm_fromdate.'  -  '.$stm_todate.'<br>';
        //echo $cb_date;

        $html_tr = '<table cellpadding="2" cellspacing="2" style="width:700px;text-align:left;">';
        echo $html_tr;

        //foreach($chargebacks_debited_array as $chargeback_row) {
        $rec_count = count($count_chargebacks_debited);
        //echo '<br>'.$rec_count.'<br>';

        //Copy only the allocated_to values into another array
        $i = 0;
        foreach ($count_chargebacks_debited as $cb_debited)
        {
                //array_push($count_chargebacks_debited_copy, $cb_debited['allocated_to']);
                $count_chargebacks_debited_copy[$i] = $cb_debited['allocated_to'];
                //echo $count_chargebacks_debited_copy[$i];
                $i++;
        }

        $rc = 0;

        $allocated_to = 0;

        if ($rec_count > 0) {

                $cnt_allocated_to = 0;
                $html_tr = '<tr>';
                $colspan_str = "";
                $tr_row_count = 0;

                foreach($count_chargebacks_debited as $chargeback_row) {

                        if ($allocated_to != $chargeback_row['allocated_to']) {

                                $allocated_to =  $chargeback_row['allocated_to'];
                                $cnt_allocated_to = 0;

                                //Count the number of rows to print
                                $tr_row_count++;
                                $tr_class = '';
                                if ($tr_row_count == 18) {
                                        $tr_class = ' page_break_before';
                                }

                                //Print the allocated_to merchant name
                                $tr = '<tr>
                                <td colspan="4" class="note_text'.$tr_class.'" style="width:100%;padding:0px;"><span><i>('.$chargeback_row['merchant_name'].')</i></span></td>
                                </tr><tr>';
                        
                                echo $tr;

                        }

                        //Get the colspan for the last row
                        $cnt_allocated_to++;
                        $cnt_allocated_value = count(array_keys($count_chargebacks_debited_copy,$chargeback_row['allocated_to']));

                        //$cnt_allocated_value_rep = array_count_values($count_chargebacks_debited_copy);
                        //$cnt_allocated_value = $counts[$chargeback_row['allocated_to']];

                        //echo '<br><br> count_allocated_value = '.$cnt_allocated_value.'<br><br>';
                        //echo '<br><br> count_allocated_to = '.$cnt_allocated_to.'<br><br>';
                        
                        //Check if it is the last item of each set of allocated_to ids
                        $last_row_colspan = 1;
                        if ($cnt_allocated_value == $cnt_allocated_to) {
                                $last_row_colspan = getLastRowColspan($chargeback_row['allocated_to']);
                        }
                        $colspan_str = 'colspan="'.$last_row_colspan.'"';

                        $cb_date = date('Y-m-d', strtotime($chargeback_row['chargeback_letter_date']));

                        $cols++;
                        $rc++;
                        $td = '<td '.$colspan_str.' class="bgrowoddd titleitems" style="position:relative;width:25%;">'.$cb_date.' - '.$chargeback_row['transaction_id'].' - '.$chargeback_row['chargeback_currency'].'</td>';
                        
                        if ($cols == 4) {
                                
                                //Check if it is the last item of each set of allocated_to ids
                                if ($cnt_allocated_value == $cnt_allocated_to) {
                                        $td = $td.'</tr>';
                                } else {
                                        $tr_row_count++;
                                        $tr_class = '';
                                        if ($tr_row_count == 18) {
                                                $tr_class = ' class="page_break_before"';
                                        }
                                        $td = $td.'</tr><tr'.$tr_class.'>';
                                }

                                //echo '<br>'.$cnt_allocated_value.' == '.$cnt_allocated_to.' ('.$allocated_to.')';

                                //Check if it is the last item
                                /*
                                if ($rc == $rec_count) {
                                        $td = $td.'</tr>';
                                } else {
                                        $td = $td.'</tr><tr>';
                                }
                                */
                                
                                $cols = 0;
                        }

                        echo $td;

                }
        } else {
                echo '<td></td>';
        }

        $html_tr = '</table>';

        echo $html_tr;

}

function getLastRowColspan($allocated_to_val) {

        $last_row_colspan = 1;

        //Get the number of allocated_to items individually
        $count_allocated_to = count(array_keys($count_chargebacks_debited_copy,$allocated_to_val));
        
        //Calculate the last row colspan
        if ($count_allocated_to > 4) {
                $no_rows = (int)($count_allocated_to / 4);
                $td_cols = $no_rows * 4;
                $rem_cols = $count_allocated_to - $td_cols;
        } else {
                $rem_cols = $count_allocated_to;
        }

        //LAST ROW COLSPAN
        if ($rem_cols == 3) {
                $last_row_colspan = 2;
        }
        if ($rem_cols == 2) {
                $last_row_colspan = 3;
        }
        if ($rem_cols == 1) {
                $last_row_colspan = 4;
        }

        return $last_row_colspan;

}

$html = '<br/><table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;background-color: #dcdcdb;"><tr><td>

<table class="page_color" cellpadding="0" cellspacing="0" style="width:700px;background-color: #dcdcdb;">
<tr>
<td style="width:50%;">
    <table cellpadding="2" cellspacing="0" style="width:100%;background-color: #dcdcdb;">
    <tr>
        <td style="border-bottom: 1px solid #82a29c;text-align:left;padding-left:3px;">
            <span style="font-size:14px; color: #82a29c;">NOTES</span>
        </td>
    </tr>
    <tr>
        <td style="padding-left:3px;">
        <table cellpadding="2" style="font-size:8px; color: #997183;padding:0; margin:0;text-align:left;">
        <tr><td> > Timezone used for reporting: (<div style="display:inline-block;" id="timezone_div">'.$tz_value_str.'</div>)</td></tr>
        <tr><td> > Merchant Discount Rate is levied on Total Sales in the Settlement Currency</td></tr>
        <tr><td> > Rolling Reserve projection</td></tr>
        <tr><td> > Remittance Bank Charges are marked for Merchant\'s account</td></tr>
        <tr><td> > Conversion is done at Association rate and is passed on Nett</td></tr>
    </table>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 1px solid #82a29c;text-align:left;padding-left:3px;">
            <span style="font-size:14px; color: #82a29c;">SETTLEMENT ENTITY</span>
        </td>
    </tr>
    <tr><td style="width:100%;height:10px;"></td></tr>
    <tr>
        <td style="text-align:left;width:100%;"><span style="font-size:9px; color: #997183;padding-left:5px;">'.$sentity.'</span></td>
    </tr>
    <tr><td style="width:100%;height:15px;"></td></tr>
    </table>
</td>
<td style="width:50%;">
    <table cellpadding="2" cellspacing="0" style="width:100%;background-color: #dcdcdb;text-align:left;">
    <tr>
    <td style="position:relative;border-bottom: 1px solid #82a29c;padding-left:3px;">
        <span style="font-size:14px; color: #82a29c;">PAYABLE TO</span>
    </td>
    </tr>
    <tr>
        <td style="border-left:1px solid #82a29c;width:100%;">
                <table class="payable_table" cellpadding="1" style="width:100%;font-size:9px; color: #82a29c;">
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Beneficiary Name</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['beneficiary_name'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Beneficiary Address</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['beneficiary_address'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Bank Name</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['bank_name'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Bank Address</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['bank_address'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Beneficiary Bank Code</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['beneficiary_bank_code'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Bank Swift Code</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['bank_swift_code'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Account Number</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['account_number'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">IBAN</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['iban'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Settlement Currency</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$assoc_array['settlement_currency'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Payment Details</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">'.$yearnow.'/'.$assoc_array['statement_no'].'/'.$assoc_array['merchant_mapping_ref_code'].'</td>
                        </tr>
                        <tr><td class="a_left pay_cols_left" style="padding-left:3px;">Payment Details 2</td>
                        <td class="a_left pay_cols_right" style="color:#997183;">T24 Card Processing</td>
                        </tr>
                </table>
        </td>
    </tr>
    </table>
</td> 
</tr>
</table>

</td></tr>
</table>

<table style="background-color: #565757;" cellpadding="3" style="width:700px;text-align:left;">
<tr>
       <td colspan="2" style="line-height: 5px;"></td>
</tr>
<tr>
        <td style="width:50%;"><img src="./images/fullheader3.png" width="305" height="auto"></td>
        <td style="width:50%;"><img src="./images/fullheader4.png" width="305" height="auto"></td>
</tr>
<tr>
<td colspan="2">
        <table style="background-color: #565757;" cellpadding="3" style="width:60%;text-align:left;">
        <tr>
        <td style="font-size: 7px;line-height: 10px;color:#ffffff;text-align:left;">
                <span>
                        <img src="./images/mail.png" width="42" height="10">&nbsp;
                </span> settlement.acq@transact24.com &nbsp;
                <span> <img src="./images/phone.png" width="10" height="10"> </span>
                <span style="color:#46a694;font-weight:bold;">&nbsp;Mauritius</span> +230 483 5356 |
                <span style="color:#46a694;font-weight:bold;">&nbsp;HK</span> +852 2851 0145 |
                <span style="color:#46a694;font-weight:bold;">&nbsp;UK</span> +44 20 3769 3640 |
                <span style="color:#46a694;font-weight:bold;">&nbsp;USA</span> +1 844 710 3474 |
                <span style="color:#46a694;font-weight:bold;">South Africa</span> +27 12 004 1770 
        </td>
        </tr>
        <tr>
        <td style="position:relative;font-size: 7px; color:#ffffff;text-align:left;">
                <span style="color: #46a694;"><b>Transact 24</b> </span> relates to the Transact24 Group, which comprises Transact24 Limited as holding company and its group of companies.
        </td>
        </tr>
        <tr>
        <td style="position:relative;font-size: 7px; color:#ffffff;text-align:left;">
                <span style="color: #46a694;"><b>Transact 24 (UK) Limited </b> </span>(part of the Transact24 Group), is an Authorised Electronic Money Institution, authorised by the Financial Conduct Authority (United Kingdom) to carry on electronic money <br/>services activities under the Electronic Money Regulations 2011 (Register Reference: 900538).
        </td>
        </tr>
        <tr>
        <td style="position:relative;font-size: 7px; color:#ffffff;text-align:left;">
                <span style="color: #46a694;"><b>Transact 24 Acquiring (Mauritius) Limited </b> </span>(part of the Transact24 Group), is authorised under a Payment Intermediary Services Licence (granted by the Financial Services Commission Mauritius pursuant to the Financial Services Act 2007 of the Republic of Mauritius), to provide payment intermediary services as a specialised financial service (Licence Number: C116015487 and Code: FS-2.9).
        </td>
        </tr> 
        <tr>
        <td colspan="2" style="line-height: 5px;"></td>
        </tr>
        </table>
</td>
</tr>
</table>

</td>
</tr>
</table> <!-- TABLE TO CENTER STATEMENT -->
<br/>
</br>

';

echo $html;

?>

</td>
</tr>
</table> <!-- end of Sub Table -->

</td>
</tr>
</table> <!-- end of Main Table -->

</body>
</html>




